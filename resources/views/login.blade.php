<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
  <title>Crystalized - Login</title>
  <meta name="description" content="This is an example of a meta description.">
  {{--<link rel="stylesheet" href="{{asset('css/admin/styles.css')}}">--}}
</head>
<body>
<div id="root"></div>
{{--<script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>--}}
<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>
