const initialState = {
  loggedIn       : false,
  loading        : false,
  loadingBackdrop: false,
  loadingPage    : false,
  errorMessages  : null,
  snackbarAlert  : {
    open     : false,
    typeAlert: 'success',
    message  : ''
  },
  alertDialog    : {
    open   : false,
    content: '',
  },
  permissions: []
};

const globalReducer = (state = initialState, action) => {
  switch (action.type) {

    case "PERMISSIONS":
      return {
        ...state,
        permissions: action.payload
      };

    case "ALERT_DIALOG":
      return {
        ...state,
        alertDialog: {
          ...state.alertDialog,
          ...action.payload
        },
      };

    case "SNACKBAR_ALERT":
      return {
        ...state,
        snackbarAlert: {
          ...state.snackbarAlert,
          ...action.payload,
        },
      };

    case "HELPER":
      return {...state, errorMessages: action.errorMessages};

    case "RESET_HELPER":
      return {...state, errorMessages: null};

    case "LOADING":
      return {...state, loading: action.payload};

    case "LOADING_BACKDROP":
      return {...state, loadingBackdrop: action.payload};

    case "LOADING_PAGE":
      return {...state, loadingPage: action.payload};

    case "LOGGED_IN":
      return {...state, loggedIn: action.payload};

    default:
      return state;
  }
};

export default globalReducer;
