const form = {
  form: {
    email   : "admin@mail.com",
    password: "password",
  }
};

const authReducer = (state = {
  ...form
}, action) => {
  switch (action.type) {

    case 'ON_CHANGE':
      const {name, value} = action;
      return {
        ...state,
        form: {
          ...state.form,
          [name]: value
        }
      };

    case 'RESET_FORM':
      return {
        ...form
      };

    default:
      return state;
  }
};

export default authReducer;
