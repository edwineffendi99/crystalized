const form = {
  form: {
    name          : "",
    permissions_id: [],
    active        : false
  }
};

const initialState = {
  rows              : [],
  meta              : {
    current_page: 0,
    per_page    : 5,
    total       : 0
  },
  dataAll           : [],
  dataModules       : [],
  groupedPermissions: {},
  ...form
};

const roleReducer = (state = initialState, action) => {
  switch (action.type) {

    case "GET_ROLE":
      return {
        ...state,
        rows: action.payload.data,
        meta: {
          ...action.payload.meta,
          current_page: parseInt(action.payload.meta.current_page) - 1
        }
      };

    case "RESET_ROWS_ROLE":
      return {...state, rows: []};

    case "RESET_META_ROLE":
      return {...state, meta: {...state.meta, current_page: 0}};

    case 'GET_DATA_ALL':
      return {
        ...state,
        dataAll: action.payload
      };

    case 'GET_MODULES_ALL':

      let permissions       = [];
      let groupedPermission = {};
      action.payload.map(n => {
        n.permissions.map((item) => {
          permissions.push(item.id);
        });
        groupedPermission[n.skey] = permissions;
        permissions               = [];
      });

      return {
        ...state,
        dataModules       : action.payload,
        groupedPermissions: groupedPermission
      };

    case 'SHOW_DATA_ROLE':
      return {
        ...state,
        form: {
          ...state.form,
          name          : action.payload.name,
          permissions_id: action.payload.permissions,
          active        : action.payload.active
        }
      };

    case 'ON_CHANGE':
      const {name, value} = action;
      return {
        ...state,
        form: {
          ...state.form,
          [name]: value
        }
      };

    case "SELECTED_MODULE":

      if (action.payload.target.checked) {
        let newSelecteds      = [];
        let permissions       = [];
        let groupedPermission = {};
        state.dataModules.map(n => {
          n.permissions.map((item) => {
            permissions.push(item.id);
          });
          groupedPermission[n.skey] = permissions;
          permissions               = [];
          state.groupedPermissions  = groupedPermission;
          if (n.skey === action.payload.target.name) {
            newSelecteds = n.permissions.map(e => e.id);
          }
        });

        return {
          ...state,
          form: {
            ...state.form,
            permissions_id: [...new Set([...state.form.permissions_id, ...newSelecteds])]
          }
        };
      }

      let toRemove         = state.groupedPermissions[action.payload.target.name];
      let setPermissionsId = state.form.permissions_id;
      toRemove.map(item => {
        let index = setPermissionsId.indexOf(item);
        if (index >= 0) {
          setPermissionsId.splice(index, 1);
        }
      });

      return {
        ...state,
        form: {
          ...state.form,
          permissions_id: setPermissionsId
        }
      };

    case "SELECTED_PERMISSION":
      const {permissions_id} = state.form;
      const selectedIndex    = permissions_id.indexOf(action.payload);
      let newSelected        = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(permissions_id, action.payload);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(permissions_id.slice(1));
      } else if (selectedIndex === permissions_id.length - 1) {
        newSelected = newSelected.concat(permissions_id.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          permissions_id.slice(0, selectedIndex),
          permissions_id.slice(selectedIndex + 1)
        );
      }
      return {
        ...state, form: {...state.form, permissions_id: newSelected}
      };

    case "RESET_SET_SELECTED":
      return {...state, selected: []};

    case 'RESET_FORM':
      return {...state, ...form};

    case 'INITIAL_STATE':
      return initialState;

    default:
      return state;
  }
};

export default roleReducer;
