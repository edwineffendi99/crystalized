import globalReducer from './globalReducer';
import authReducer   from './authReducer';
import userReducer   from './userReducer';
import roleReducer   from './roleReducer';

const index = {
  globalReducer,
  authReducer,
  userReducer,
  roleReducer,
};

export default index;
