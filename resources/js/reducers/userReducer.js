const form = {
  form: {
    name       : "",
    email      : "",
    password   : "",
    address    : "",
    profile_pic: null,
    role_id    : '',
    active     : false
  }
};

const initialState = {
  rows           : [],
  meta           : {
    current_page: 0,
    per_page    : 5,
    total       : 0
  },
  imagePreviewUrl: 'https://via.placeholder.com/160x160',
  ...form
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {

    case "GET_USERS":
      return {
        ...state,
        rows: action.payload.data,
        meta: {
          ...action.payload.meta,
          current_page: parseInt(action.payload.meta.current_page) - 1
        }
      };

    case "RESET_ROWS_USERS":
      return {...state, rows: []};

    case "RESET_META_USERS":
      return {...state, meta: {...state.meta, current_page: 0}};

    case 'SHOW_DATA_USERS':
      return {
        ...state,
        imagePreviewUrl: action.payload.profile_pic,
        form           : {
          ...state.form,
          name   : action.payload.name,
          email  : action.payload.email,
          address: action.payload.address,
          role_id: action.payload.role.skey,
          active : action.payload.active
        }
      };

    case 'ON_CHANGE':
      const {name, value} = action;
      return {
        ...state,
        form: {
          ...state.form,
          [name]: value
        }
      };

    case 'ON_CHANGE_FILE':
      return {
        ...state,
        imagePreviewUrl: action.previewUrl,
        form           : {
          ...state.form,
          [action.name]: action.value
        }
      };

    case 'RESET_FORM':
      return {
        ...state,
        imagePreviewUrl: 'https://via.placeholder.com/160x160',
        ...form
      };

    case 'INITIAL_STATE':
      return initialState;

    default:
      return state;
  }
};

export default userReducer;
