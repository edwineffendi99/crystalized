import React                                                        from 'react';
import PropTypes                                                    from "prop-types";
import {AppBar, Badge, IconButton, makeStyles, Toolbar, Typography} from '@material-ui/core';
import MenuIcon                                                     from '@material-ui/icons/Menu';
import AccountCircle                                                from '@material-ui/icons/AccountCircle';
import MailIcon                                                     from '@material-ui/icons/Mail';
import NotificationsIcon                                            from '@material-ui/icons/Notifications';
import MoreIcon                                                     from '@material-ui/icons/MoreVert';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  appBar        : {
    boxShadow                   : 'none',
    [theme.breakpoints.up('sm')]: {
      width          : `calc(100% - ${drawerWidth}px)`,
      marginLeft     : drawerWidth,
      backgroundColor: '#f4f6f8',
    },
  },
  menuButton    : {
    marginRight                 : theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  large         : {
    width : theme.spacing(5),
    height: theme.spacing(5),
  },
  grow          : {
    flexGrow: 1,
  },
  sectionDesktop: {
    display                     : 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile : {
    display                     : 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  colorBlack    : {
    color: '#000'
  }
}));

function Topbar(props) {
  const {handleDrawerToggle} = props;
  const classes              = useStyles();
  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon/>
        </IconButton>
        <Typography variant="h6" className={classes.grow}>
          Crystalized
        </Typography>
        <div className={classes.sectionDesktop}>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <Badge badgeContent={4} color="secondary">
              <MailIcon classes={{root: classes.colorBlack}}/>
            </Badge>
          </IconButton>
          <IconButton aria-label="show 17 new notifications" color="inherit">
            <Badge badgeContent={17} color="secondary">
              <NotificationsIcon classes={{root: classes.colorBlack}}/>
            </Badge>
          </IconButton>
          <IconButton
            edge="end"
            aria-label="account of current user"
            // aria-controls={menuId}
            aria-haspopup="true"
            // onClick={handleProfileMenuOpen}
            color="inherit"
          >
            <AccountCircle classes={{root: classes.colorBlack}}/>
          </IconButton>
        </div>
        <div className={classes.sectionMobile}>
          <IconButton
            aria-label="show more"
            // aria-controls={mobileMenuId}
            aria-haspopup="true"
            onClick={handleDrawerToggle}
            color="inherit"
          >
            <MoreIcon/>
          </IconButton>
        </div>
      </Toolbar>
    </AppBar>
  );
}

Topbar.propTypes = {
  handleDrawerToggle: PropTypes.func,
};

export default Topbar
