import React                          from 'react';
import {Link, makeStyles, Typography} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4, 0)
  }
}));

function Footer() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="body1">
        &copy;{' '}
        <Link
          component="a"
          href="https://crystalized/"
          target="_blank">
          Crystalized
        </Link>
        . 2020
      </Typography>
      <Typography variant="caption">
        Created with love for the environment. By designers and developers who
        love to work together in offices!
      </Typography>
    </div>
  );
}

export default Footer;
