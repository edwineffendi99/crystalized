import React, {Fragment, useState} from 'react';
import {useHistory}                from "react-router-dom";
import PropTypes                   from 'prop-types';

import {Collapse, List, ListItem, ListItemIcon, ListItemText, makeStyles} from '@material-ui/core';
import ExpandMore                                                         from '@material-ui/icons/ExpandMore';

import DataList  from './DataList';
import {signOut} from "../../../../../../actions/authAction";
import Can       from "../../../../../../helpers/Can";

const useStyles = makeStyles(theme => ({
  toolbar   : theme.mixins.toolbar,
  colorWhite: {
    color: '#fff'
  },
  icon      : {
    minWidth: 42,
    color   : '#fff'
  },
  expandMore: {
    transform : 'rotate(180deg)',
    color     : '#fff',
    transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
  },
  expandLess: {
    color     : '#fff',
    transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
  },
  dot       : {
    width       : '4px',
    height      : '4px',
    background  : '#fff',
    borderRadius: '100%',
    margin      : 'auto 12px auto auto',
  }
}));

const submenu = () => {
  let newObj = {};
  DataList.map(e => e.submenu && (newObj['open' + e.id] = false));
  return newObj;
};

const SidebarNav = ({handleDrawerToggle}) => {

  const classes       = useStyles();
  let [open, setOpen] = useState(submenu);
  let history         = useHistory();

  const handleOnClick = (item) => {
    if (item.submenu) {
      setOpen({...open, ['open' + item.id]: !open['open' + item.id]});
    } else if (item.id === 5) {
      signOut();
    } else {
      handleDrawerToggle && handleDrawerToggle();
      history.push(item.path, {slug: 0});
    }
  };

  const renderListItem = (item) => {
    return (
      <Fragment>
        <ListItem button onClick={() => handleOnClick(item)}>
          <ListItemIcon classes={{root: classes.icon}}>{item.icon}</ListItemIcon>
          <ListItemText classes={{primary: classes.colorWhite}}>{item.label}</ListItemText>
          {item.submenu &&
          <ExpandMore className={open['open' + item.id] ? classes.expandMore : classes.expandLess}/>}
        </ListItem>
        {
          item.submenu &&
          <Collapse in={open['open' + item.id]} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {
                item.submenu.map((sub, key) =>
                  <ListItem key={key} button className={classes.nested} onClick={() => handleOnClick(sub)}>
                    <ListItemIcon classes={{root: classes.icon}}>
                      <div className={classes.dot}/>
                    </ListItemIcon>
                    <ListItemText classes={{primary: classes.colorWhite}}>
                      {sub.label}
                    </ListItemText>
                  </ListItem>
                )
              }
            </List>
          </Collapse>
        }
      </Fragment>
    )
  };

  return (
    <Fragment>
      <div className={classes.toolbar}/>
      <List>
        {
          DataList.map((item, index) => (
            <Fragment key={index}>
              {
                item.id !== 1 && item.id !== 4 && item.id !== 5 ?
                  <Can type={`${item.key}:browse`}>
                    {renderListItem(item)}
                  </Can>
                  :
                  renderListItem(item)
              }
            </Fragment>
          ))
        }
      </List>
    </Fragment>
  );
};

SidebarNav.propTypes = {
  className: PropTypes.string,
};

export default SidebarNav;
