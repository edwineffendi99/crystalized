import React                    from "react";
import SubscribeIcon            from "@material-ui/icons/PersonAdd";
import ViewIcon                 from "@material-ui/icons/AccessTime";
import HistoryIcon              from "@material-ui/icons/History";
import DashboardIcon            from '@material-ui/icons/Dashboard';
import ShoppingCartIcon         from '@material-ui/icons/ShoppingCart';
import ExitToAppIcon            from '@material-ui/icons/ExitToApp';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import AssignmentIndIcon        from '@material-ui/icons/AssignmentInd';

const DataList = [
  {
    id   : 1,
    key  : 'dashboard',
    label: 'Dashboard',
    path : '/admin/dashboard',
    icon : <DashboardIcon/>
  },
  {
    id   : 2,
    key  : 'user',
    label: 'User',
    path : '/admin/user',
    icon : <SupervisedUserCircleIcon/>
  },
  {
    id   : 3,
    key  : 'role',
    label: 'Role',
    path : '/admin/role',
    icon : <AssignmentIndIcon/>
  },
  {
    id     : 4,
    key    : 'campaign',
    label  : 'Campaign',
    path   : '/dashboard/campaign',
    icon   : <ShoppingCartIcon/>,
    submenu: [
      {
        label: 'View',
        path : '/dashboard/campaign/view',
        icon : <ViewIcon/>
      },
      {
        label: 'Subscribe',
        path : '/dashboard/campaign/subscription',
        icon : <SubscribeIcon/>
      },
      {
        label: 'History',
        path : '/dashboard/campaign/history',
        icon : <HistoryIcon/>
      }
    ]
  },
  {
    id   : 5,
    key  : 'logout',
    label: 'Logout',
    path : '/dashboard/logout',
    icon : <ExitToAppIcon/>
  },
];


export default DataList
