import React                                  from 'react';
import PropTypes                              from 'prop-types';
import {Drawer, Hidden, makeStyles, useTheme} from '@material-ui/core';

import SidebarNav from './components/SidebarNav';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer     : {
    [theme.breakpoints.up('sm')]: {
      width     : drawerWidth,
      flexShrink: 0,
    },
  },
  toolbar    : theme.mixins.toolbar,
  drawerPaper: {
    width     : drawerWidth,
    background: 'linear-gradient(180deg, rgba(238,69,64,1) 0%, rgba(199,44,65,1) 50%, rgba(128,19,54,1) 100%)'
  }
}));

function Sidebar(props) {
  const {container, mobileOpen, handleDrawerToggle} = props;
  const classes                                     = useStyles();
  const theme                                       = useTheme();

  return (
    <nav className={classes.drawer}>
      <Hidden smUp implementation="css">
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{paper: classes.drawerPaper}}
          ModalProps={{keepMounted: true}}
        >
          <SidebarNav handleDrawerToggle={handleDrawerToggle}/>
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{paper: classes.drawerPaper}}
          variant="permanent"
          open
        >
          <SidebarNav/>
        </Drawer>
      </Hidden>
    </nav>
  );
}

Sidebar.propTypes = {
  mobileOpen        : PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  container         : PropTypes.instanceOf(typeof Element === 'undefined' ? Object : Element),
};

export default Sidebar;
