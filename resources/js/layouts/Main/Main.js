import React                     from 'react';
import {CssBaseline, makeStyles} from '@material-ui/core';

import {Footer, Sidebar, Topbar} from './components';
import Bredcrumb                 from "../../components/Bredcrumb";
import SnackbarAlert             from "../../helpers/SnackbarAlert";
import AlertDialog               from "../../helpers/AlertDialog";
import LoadingBackdrop           from "../../helpers/LoadingBackdrop";

const useStyles = makeStyles(theme => ({
  root   : {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding : theme.spacing(8, 3, 1),
  }
}));

const Main = props => {
  const {children}                  = props;
  const classes                     = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      <AlertDialog/>
      <SnackbarAlert/>
      <LoadingBackdrop/>
      <CssBaseline/>
      <Topbar handleDrawerToggle={handleDrawerToggle}/>
      <Sidebar mobileOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle}/>
      <main className={classes.content}>
        <Bredcrumb {...props}/>
        {children}
        <Footer/>
      </main>
    </div>
  );
};

export default Main;
