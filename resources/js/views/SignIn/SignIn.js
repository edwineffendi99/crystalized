import React         from 'react';
import {useSelector} from "react-redux";

import {
  Avatar,
  Box, Button,
  Checkbox,
  Container,
  CssBaseline,
  FormControlLabel,
  Grid,
  Link,
  makeStyles,
  Typography
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import {signIn}       from "./../../actions/authAction";
import {handleChange} from "./../../actions/globalAction";
import TextInput      from './../../components/TextInput';
import ButtonProgress from "./../../components/ButtonProgress";
import SnackbarAlert  from "../../helpers/SnackbarAlert/SnackbarAlert";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  root  : {
    height                      : '100%',
    [theme.breakpoints.up('md')]: {
      display       : 'flex',
      flexDirection : 'column',
      alignItems    : 'center',
      justifyContent: 'center',
    },
    backgroundColor             : '#fff'
  },
  paper : {
    paddingTop   : theme.spacing(10),
    display      : 'flex',
    flexDirection: 'column',
    alignItems   : 'center',
  },
  avatar: {
    margin         : theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form  : {
    width    : '100%',
    marginTop: theme.spacing(1),
  },
}));

export default function SignIn() {
  const classes = useStyles();
  const counter = useSelector(state => state);
  const {form}  = counter.authReducer;

  const handleSubmit = (e) => {
    e.preventDefault();
    signIn(form)
  };

  return (
    <Container component="main" maxWidth="xs" className={classes.root}>
      <SnackbarAlert/>
      <CssBaseline/>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in Crystalized
        </Typography>
        <form className={classes.form} onSubmit={(e) => handleSubmit(e)} noValidate>
          <TextInput
            label="Email address"
            name="email"
            value={form.email}
            onChange={handleChange}
          />
          <TextInput
            label="Password"
            name="password"
            value={form.password}
            type="password"
            onChange={handleChange}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary"/>}
            label="Remember me"
          />
          <ButtonProgress fullWidth type="submit">Sign In</ButtonProgress>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright/>
      </Box>
    </Container>
  );
}
