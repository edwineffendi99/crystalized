import React, {useEffect} from "react";
import EnhancedTable      from '../../components/EnhancedTable';
import {getAllData}       from "../../actions/roleAction";
import {Grid}             from '@material-ui/core';

function Role() {
  const url     = 'roles';
  const pathUrl = 'role';
  const getType = 'ROLE';

  const dataActive = [
    {
      label: "-ALL-",
      value: ""
    }, {
      label: "Active",
      value: 1
    }, {
      label: "Inactive",
      value: 0
    }
  ];

  const headCells = [
    {id: 'name', numeric: false, disablePadding: true, label: 'Name', search: true, searchType: 'text'},
    {
      id            : 'active',
      numeric       : false,
      disablePadding: false,
      label         : 'Status',
      search        : true,
      searchType    : 'select',
      data          : dataActive
    },
  ];

  useEffect(() => {
    getAllData(`/api/${url}/all`);
  }, []);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <EnhancedTable label={`Roles`} getType={getType} url={url} pathUrl={pathUrl} headCells={headCells}/>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default Role;
