import React, {useEffect}         from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams}    from "react-router-dom";

import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Grid,
  makeStyles,
  Paper,
  Typography
} from '@material-ui/core';

import {errorOn, handleChange, messageError, showData} from "../../../actions/globalAction";
import TextInput                                       from "../../../components/TextInput";
import SwitchInput                                     from "../../../components/SwitchInput";
import {getModules, storeUpdateData}                   from "../../../actions/roleAction";
import ButtonProgress                                  from "../../../components/ButtonProgress";

const useStyles = makeStyles(theme => ({
  root         : {
    display: 'flex'
  },
  paper        : {
    width       : '100%',
    padding     : theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  button       : {
    marginLeft : theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  wrapperButton: {
    marginLeft : theme.spacing(-1),
    marginRight: theme.spacing(-1),
  },
  title        : {
    marginBottom: theme.spacing(1),
  }
}));

function FormControls() {
  const classes             = useStyles();
  const history             = useHistory();
  const dispatch            = useDispatch();
  const {type, skey}        = useParams();
  const counter             = useSelector(state => state);
  const {form, dataModules} = counter.roleReducer;
  const isSelected          = name => form.permissions_id.indexOf(name) !== -1;
  const url                 = 'roles';
  const disabled            = type === 'show' ? true : false;

  useEffect(() => {
    getModules();
    if (type !== 'store') {
      showData(url, skey, 'ROLE');
    }
    return () => {
      dispatch({type: "RESET_FORM"});
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    storeUpdateData(url, form, type, skey);
  };

  const handleCancel = (e) => {
    e.preventDefault();
    history.goBack();
  };

  const Permissions = ({id}) => {
    return (
      <FormGroup row>
        {
          dataModules &&
          dataModules.map((i, k) => {
            const perm             = i.permissions.map(a => a.id);
            const isSelectedModule = form.permissions_id.filter(e => perm.indexOf(e) !== -1).length === perm.length;
            return (
              <FormControl key={k} margin="dense" fullWidth>
                <FormControlLabel
                  disabled={disabled}
                  control={
                    <Checkbox
                      checked={isSelectedModule}
                      name={i.skey}
                      onChange={(e) => dispatch({type: 'SELECTED_MODULE', payload: e})}
                      value={i.key}/>
                  }
                  label={i.name}
                />
                {
                  i.permissions.map((item, key) => {
                      const isItemSelected = isSelected(item.id);
                      return (
                        <FormControl key={key} style={{marginLeft: 12}}>
                          <FormControlLabel
                            disabled={disabled}
                            control={
                              <Checkbox checked={isItemSelected}
                                        onChange={() => dispatch({type: 'SELECTED_PERMISSION', payload: item.id})}
                                        value={item.id}/>
                            }
                            label={item.name}
                          />
                        </FormControl>
                      )
                    }
                  )
                }
              </FormControl>
            )
          })
        }
        <FormHelperText error={errorOn(id)} variant="outlined">{messageError(id)}</FormHelperText>
      </FormGroup>
    );
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item lg={4} md={6}>
          <form onSubmit={(e) => handleSubmit(e)}>
            <Paper className={classes.paper}>
              <Typography className={classes.title} variant="h4" id="tableTitle">
                Role
              </Typography>

              <TextInput
                label="Name"
                name="name"
                value={form.name}
                disabled={disabled}
                onChange={handleChange}
                required
              />

              <Permissions id="permissions_id"/>

              <SwitchInput
                label="Status"
                name="active"
                value={form.active}
                checked={form.active}
                disabled={disabled}
                onChange={handleChange}
              />

              <div className={classes.wrapperButton}>
                {
                  type !== "show" ?
                    <ButtonProgress type="submit" className={classes.button}>Submit</ButtonProgress>
                    :
                    <Button
                      variant="contained"
                      color="primary"
                      size="medium"
                      className={classes.button}
                      onClick={(e) => history.push(`/admin/role/update/${skey}`)}>
                      Edit
                    </Button>
                }
                <Button
                  variant="contained"
                  color="primary"
                  size="medium"
                  className={classes.button}
                  onClick={(e) => handleCancel(e)}>
                  Cancel
                </Button>
              </div>
            </Paper>
          </form>
        </Grid>
      </Grid>
    </div>
  );
}

export default FormControls;
