import React, {useEffect}         from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams}    from "react-router-dom";

import {Button, Grid, makeStyles, Paper, Typography} from '@material-ui/core';

import ImageUpload                                from '../../../components/ImageUpload';
import {handleChange, handleFileChange, showData} from "../../../actions/globalAction";
import {storeUpdateData}                          from "../../../actions/userAction";
import TextInput                                  from "../../../components/TextInput";
import SwitchInput                                from "../../../components/SwitchInput";
import SelectInput                                from '../../../components/SelectInput';
import {getAllData}                               from "../../../actions/roleAction";
import ButtonProgress                             from "../../../components/ButtonProgress";

const useStyles = makeStyles(theme => ({
  root         : {
    display: 'flex'
  },
  paper        : {
    width       : '100%',
    padding     : theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  button       : {
    marginLeft : theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  wrapperButton: {
    marginLeft : theme.spacing(-1),
    marginRight: theme.spacing(-1),
  },
  title        : {
    marginBottom: theme.spacing(1),
  }
}));

function FormControls() {
  const classes                 = useStyles();
  const history                 = useHistory();
  const dispatch                = useDispatch();
  const {type, skey}            = useParams();
  const counter                 = useSelector(state => state);
  const {form, imagePreviewUrl} = counter.userReducer;
  const {dataAll}               = counter.roleReducer;
  const url                     = 'users';
  const menuItem                = dataAll ? dataAll.map(({skey: value, name: label}) => ({label, value})) : [];
  const disabled                 = type === 'show' ? true : false;

  useEffect(() => {
    getAllData('/api/roles/all');
    if (type !== 'store') {
      showData(url, skey, 'USERS');
    }
    return () => {
      dispatch({type: 'RESET_FORM'});
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    storeUpdateData(url, form, type, skey);
  };

  const handleCancel = (e) => {
    e.preventDefault();
    history.goBack();
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item lg={4} md={6}>
          <form onSubmit={(e) => handleSubmit(e)}>
            <Paper className={classes.paper}>
              <Typography className={classes.title} variant="h4" id="tableTitle">
                Users
              </Typography>

              <ImageUpload
                name="profile_pic"
                preview={imagePreviewUrl}
                disabled={disabled}
                onChange={handleFileChange}/>

              <TextInput
                label="Name"
                name="name"
                value={form.name}
                disabled={disabled}
                onChange={handleChange}
                required
              />

              <TextInput
                label="Email"
                name="email"
                value={form.email}
                disabled={disabled}
                onChange={handleChange}
                required
              />

              <TextInput
                label="Password"
                name="password"
                value={form.password}
                type="password"
                disabled={disabled}
                onChange={handleChange}
              />

              <TextInput
                label="Address"
                name="address"
                value={form.address}
                multiline
                rows="5"
                disabled={disabled}
                onChange={handleChange}
                required
              />

              <SelectInput
                name="role_id"
                label="Select Role"
                value={form.role_id}
                disabled={disabled}
                onChange={handleChange}
                menuItem={menuItem}
                required
              />

              <SwitchInput
                label="Status"
                name="active"
                value={form.active}
                checked={form.active}
                disabled={disabled}
                onChange={handleChange}
                required
              />

              <div className={classes.wrapperButton}>
                {
                  type !== 'show' ?
                    <ButtonProgress type="submit" className={classes.button}>Submit</ButtonProgress>
                    :
                    <Button
                      variant="contained"
                      color="primary"
                      size="medium"
                      className={classes.button}
                      onClick={(e) => history.push(`/admin/user/update/${skey}`)}>
                      Edit
                    </Button>
                }
                <Button
                  variant="contained"
                  color="primary"
                  size="medium"
                  className={classes.button}
                  onClick={(e) => handleCancel(e)}>
                  Cancel
                </Button>
              </div>
            </Paper>
          </form>
        </Grid>
      </Grid>
    </div>
  );
}

export default FormControls;
