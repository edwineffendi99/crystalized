import React, {useEffect} from "react";
import EnhancedTable      from '../../components/EnhancedTable';
import {getAllData}       from "../../actions/roleAction";
import {useSelector}      from "react-redux";

function User() {
  const url       = 'users';
  const pathUrl   = 'user';
  const getType   = 'USERS';
  const counter   = useSelector(state => state);
  const {dataAll} = counter.roleReducer;
  const menuItem  = dataAll ? dataAll.map(({skey: value, name: label}) => ({label, value})) : [];

  const dataRole = [
    {
      label: "-ALL-",
      value: ""
    },
    ...menuItem
  ];

  const dataActive = [
    {
      label: "-ALL-",
      value: ""
    }, {
      label: "Active",
      value: 1
    }, {
      label: "Inactive",
      value: 0
    }
  ];

  const headCells = [
    {id: 'name', numeric: false, disablePadding: true, label: 'Name', search: true, searchType: 'text'},
    {id: 'email', numeric: false, disablePadding: false, label: 'Email', search: true, searchType: 'text'},
    {id: 'address', numeric: false, disablePadding: false, label: 'Address', search: true, searchType: 'text'},
    {
      id            : 'role.name',
      numeric       : false,
      disablePadding: false,
      label         : 'Role',
      search        : true,
      searchId      : 'role_id',
      searchType    : 'select',
      data          : dataRole,
      permalink     : 'role.permalink'
    },
    {
      id            : 'active',
      numeric       : false,
      disablePadding: false,
      label         : 'Status',
      search        : true,
      searchType    : 'select',
      data          : dataActive
    },
  ];

  useEffect(() => {
    getAllData('/api/roles/all');
  }, []);

  return (
    <React.Fragment>
      <EnhancedTable label={`Users`} getType={getType} url={url} pathUrl={pathUrl} headCells={headCells}/>
    </React.Fragment>
  )
}

export default User;
