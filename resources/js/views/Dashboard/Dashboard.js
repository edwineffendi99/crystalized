import React, {useEffect}        from "react";
import {getData}                 from '../../actions/globalAction';
import {Grid, makeStyles, Paper} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  paper: {
    width       : '100%',
    padding     : theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
}));

const headCells = [
  {id: 'name', numeric: false, disablePadding: true, label: 'Name', search: false, typeSearch: 'text'},
  {id: 'email', numeric: false, disablePadding: false, label: 'Email', search: true, typeSearch: 'text'},
  {id: 'address', numeric: false, disablePadding: false, label: 'Address', search: true, typeSearch: 'text'},
  {id: 'active', numeric: false, disablePadding: false, label: 'Status', search: false, typeSearch: 'select'},
];

function Dashboard() {
  const url     = 'users';
  const classes = useStyles();

  useEffect(() => {
    getData('/api/users/index/5/name/asc');
  }, []);

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Paper className={classes.paper}>Dashboard</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>Dashboard</Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>Dashboard</Paper>
        </Grid>
      </Grid>
      {/*<EnhancedTable url={url} headCells={headCells}/>*/}
    </React.Fragment>
  )
}

export default Dashboard;
