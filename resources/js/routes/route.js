import React, {useEffect, Fragment} from 'react';
import {Redirect, Route}  from "react-router-dom";

import Layout          from "./../layouts/Main";
import Dashboard       from "./../views/Dashboard";
import User            from "./../views/User";
import UserFormControl from "./../views/User/FormControls";
import Role            from "./../views/Role";
import RoleFormControl from "./../views/Role/FormControls";

import {getPermissions, getProfile} from "../actions/authAction";
import Can                          from "../helpers/Can";

const routes = [
  {
    path     : '/admin/dashboard',
    exact    : false,
    component: Dashboard,
  },
  {
    permissionsKey: 'user',
    path          : '/admin/user',
    exact         : false,
    component     : User,
    routes        : [
      {
        path     : '/:type/:skey?',
        exact    : false,
        component: UserFormControl,
      },
    ]
  },
  {
    permissionsKey: 'role',
    path          : '/admin/role',
    exact         : false,
    component     : Role,
    routes        : [
      {
        path     : '/:type/:skey?',
        exact    : false,
        component: RoleFormControl,
      },
    ]
  },
];

function Index(props) {
  const loggedIn = localStorage.getItem('LOGGED_IN');

  if (!loggedIn) {
    return <Redirect to="/admin/login"/>
  }

  useEffect(() => {
    getProfile();
    getPermissions();
  }, []);

  const itemRoute = (route) => {
    return (
      <Route path={route.path} render={({match: {url}}) => (
        <Fragment>
          <Route exact path={url} component={route.component}/>
          {
            route.routes &&
            route.routes.map((r, k) =>
              <Route key={k} path={url + r.path} component={r.component}/>
            )
          }
        </Fragment>
      )}/>
    )
  };

  return (
    <Layout {...props}>
      {
        routes.map((route, i) =>
          route.permissionsKey ?
            <Can key={i} type={`${route.permissionsKey}:browse`}>
              {itemRoute(route)}
            </Can> :
            itemRoute(route)
        )
      }
    </Layout>
  )
}

export default Index;
