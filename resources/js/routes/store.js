import {applyMiddleware, createStore, combineReducers} from "redux";
import thunk                                           from "redux-thunk";
import backofficeReducer                               from "../reducers/index";

const rootReducer = combineReducers({
  ...backofficeReducer,
});

const middleware = applyMiddleware(thunk);
const store      = createStore(rootReducer, middleware);

export default store;
