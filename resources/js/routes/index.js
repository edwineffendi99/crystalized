import React                   from 'react';
import {Route, Router, Switch} from "react-router-dom";
import Dashboard               from './route';
import history                 from "./history";
import SignIn                  from './../views/SignIn';

function Index() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/admin/login" component={SignIn}/>
        <Route path="/admin" component={Dashboard}/>
        <Route component={() => <h1>404</h1>}/>
      </Switch>
    </Router>
  )
}

export default Index
