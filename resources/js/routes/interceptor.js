import axios   from "axios";
import store   from "./store";
import history from "./history";

let {dispatch, getState} = store;
let isRefreshing         = false;

let failedQueue = [];

const processQueue = (error, token = null) => {
  failedQueue.forEach(prom => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

function select(state) {
  return state.globalReducer.permissions.length;
}

var currentValue;

axios.interceptors.request.use((config) => {
  dispatch({type: 'LOADING', payload: true});
  if (config.method === "post") {
    dispatch({type: 'RESET_HELPER'});
  }

  // var previousValue = currentValue;
  // currentValue      = select(getState());
  // if (previousValue !== currentValue) {
  //   getPermissions();
  //   // console.log(
  //   //   'Some deep nested property changed from',
  //   //   previousValue,
  //   //   'to',
  //   //   currentValue
  //   // );
  // }

  return config;
}, error => {
  dispatch({type: 'LOADING', payload: false});
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  dispatch({type: 'LOADING', payload: false});
  dispatch({type: 'RESET_HELPER'});
  return response;
}, error => {
  const originalRequest = error.config;
  const {status}        = error.response;

  if (originalRequest.url === "/api/login") {
    dispatch({type: 'LOADING', payload: false});
    return Promise.reject(error);
  }

  if (status === 403) {
    //show message or page action forbidden
  }

  if (status === 401 && !originalRequest._retry) {

    if (isRefreshing) {
      return new Promise(function (resolve, reject) {
        failedQueue.push({resolve, reject});
      }).then(token => {
        originalRequest.headers['Authorization'] = 'Bearer ' + token;
        return axios(originalRequest);
      }).catch(err => {
        return Promise.reject(err);
      });
    }

    originalRequest._retry = true;
    isRefreshing           = true;

    return new Promise(function (resolve, reject) {
      axios.put('/api/refresh_token').then(res => {
        if (res.status === 200) {
          isRefreshing = false;
          processQueue(null, 123);
          resolve(axios(originalRequest));
        }
      }).catch(err => {
        if (error.response.status === 401) {
          history.push('/admin/login');
        }
      });
    });
  }

  if (status === 422) {
    dispatch({type: 'HELPER', errorMessages: error.response.data.data});
  }


  const message        = error.response.data.message;
  const messageGeneral = error.response.data.data.general ? ', ' + error.response.data.data.general : '';
  dispatch({
    type   : 'SNACKBAR_ALERT',
    payload: {
      open     : true,
      typeAlert: 'error',
      message  : message + messageGeneral
    }
  });

  dispatch({type: 'LOADING', payload: false});
  return Promise.reject(error);
});
