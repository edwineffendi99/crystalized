import React             from "react";
import {Redirect, Route} from "react-router-dom";

export default function ProtectedRoute({component: Component, ...rest}) {
  return (
    <Route
      {...rest}
      render={props => {
        const loggedIn = localStorage.getItem('LOGGED_IN');
        return loggedIn ? <Component {...props}/> : <Redirect to="/admin/login"/>
      }}
    />
  )
}
