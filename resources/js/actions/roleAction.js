import axios            from 'axios';
import store            from './../routes/store';
import history          from "../routes/history";
import {getPermissions} from "./authAction";

let {dispatch} = store;

export async function getAllData(url) {
  try {
    const res = await axios.get(url);
    dispatch({type: 'GET_DATA_ALL', payload: res.data.data});
  } catch (e) {
    console.log(e);
  }
}

export async function getModules() {
  try {
    const res = await axios.get(`/api/modules/all`);
    dispatch({type: 'GET_MODULES_ALL', payload: res.data.data});
  } catch (e) {
    console.log(e);
  }
}

export async function storeUpdateData(url, data, type, skey = '') {

  const newUrl = type !== 'store' ? `/api/${url}/${skey}` : `/api/${url}`;
  const method = type !== 'store' ? 'PATCH' : 'POST';

  try {
    const res = await axios({method: method, url: newUrl, data: data});
    dispatch({type: 'SNACKBAR_ALERT', payload: {open: true, typeAlert: 'success', message: res.data.message}});
    dispatch({type: 'RESET_GET_META_ROLE'});
    getPermissions();
    history.push('/admin/role');
  } catch (e) {
    console.log('error');
  }
}
