import axios   from "axios";
import store   from "../routes/store";
import history from "../routes/history";

let {dispatch} = store;

const signIn = async (data) => {
  dispatch({type: 'RESET_HELPER'});
  try {
    await axios.post('/api/login', data, {headers: {'Accept': 'application/json'}});
    dispatch({type: 'LOGGED_IN', payload: true});
    localStorage.setItem('LOGGED_IN', true);
    getProfile();
    history.push('/admin/user');
  } catch (e) {
    dispatch({type: 'LOGGED_IN', payload: false});
    const {message} = e.response.data;
    dispatch({
      type   : 'SNACKBAR_ALERT',
      payload: {
        open     : true,
        typeAlert: 'error',
        message  : message
      }
    });
  }
};

const signOut = async () => {
  dispatch({type: 'LOADING_BACKDROP', payload: true});
  try {
    await axios.get('/api/logout', {headers: {'Accept': 'application/json'}});
    dispatch({type: 'LOGGED_IN', payload: false});
    dispatch({type: 'LOADING_BACKDROP', payload: false});
    dispatch({type: 'INITIAL_STATE'});
    localStorage.clear();
    history.push('/admin/login');
  } catch (e) {
    dispatch({type: 'LOADING_BACKDROP', payload: false});
  }
};

const getProfile = async () => {
  try {
    await axios.get('/api/profile', {headers: {'Accept': 'application/json'}});
    dispatch({type: 'LOGGED_IN', payload: true});
  } catch (e) {
    dispatch({type: 'LOGGED_IN', payload: false});
  }
};

const getPermissions = async () => {
  try {
    const res = await axios.get('/api/permissions/all', {headers: {'Accept': 'application/json'}});
    dispatch({type: 'PERMISSIONS', payload: res.data.data});
  } catch (e) {
    dispatch({type: 'LOGGED_IN', payload: false});
  }
};

export {
  getPermissions,
  getProfile,
  signIn,
  signOut
};
