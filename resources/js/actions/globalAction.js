import axios from 'axios';
import store from './../routes/store';

let {dispatch, getState} = store;

export async function getData(url, type) {
  dispatch({type: `RESET_ROWS_${type}`});
  try {
    const res = await axios.get(url);
    dispatch({type: `GET_${type}`, payload: res.data});
  } catch (e) {
    console.log(e);
  }
}

export const handleChange = (event, names, percent) => {
  const target     = event.target;
  const max        = 100;
  const value      = target.type !== 'checkbox' ? target.value : target.checked;
  const name       = target.name;
  const valPercent = isNaN(value) ? 0 : Math.max(Math.min(value, max), 0);

  dispatch({
    type : 'ON_CHANGE',
    name : target.name ? name : names,
    value: percent ? valPercent : value,
  });
};

export const handleFileChange = (event) => {
  event.preventDefault();

  let reader        = new FileReader();
  let {name, files} = event.target;
  let file          = files[0];

  reader.onloadend = () => {
    dispatch({
      type      : 'ON_CHANGE_FILE',
      previewUrl: reader.result,
      name      : name,
      value     : file,
    });
  };

  reader.readAsDataURL(file);
};

export async function showData(url, skey, type) {
  try {
    const res = await axios.get(`/api/${url}/${skey}`);
    dispatch({type: `SHOW_DATA_${type}`, payload: res.data.data});
  } catch (e) {
    console.log('error');
  }
}

export async function deleteData(url, skey, getType) {
  try {
    const res = await axios.delete(`/api/${url}/${skey}`);
    getData(`/api/${url}/index/5/created_at/desc`, getType);
    dispatch({
      type   : 'SNACKBAR_ALERT',
      payload: {open: true, typeAlert: 'success', message: `${res.data.message}`}
    });
    dispatch({type: 'ALERT_DIALOG', payload: {open: false}});
  } catch (e) {
    console.error(e);
  }
}

export async function deleteMultiData(url, skeys, getType, setSelected) {
  try {
    const res = await axios.post(`/api/${url}/delete_multi`, {skeys: skeys});
    getData(`/api/${url}/index/5/created_at/desc`, getType);
    dispatch({
      type   : 'SNACKBAR_ALERT',
      payload: {open: true, typeAlert: 'success', message: `${res.data.message}`}
    });
    dispatch({type: 'ALERT_DIALOG', payload: {open: false}});
    setSelected([]);
  } catch (e) {
    console.error(e);
  }
}

export function messageError(name) {
  const {errorMessages} = getState().globalReducer;
  if (errorMessages) {
    if (typeof errorMessages[name] !== 'object') {
      return errorMessages[name];
    }
    return errorMessages[name];
  }
}

export function errorOn(name) {
  const {errorMessages} = getState().globalReducer;
  if (errorMessages) {
    if (typeof errorMessages[name] !== 'object') {
      return false;
    } else {
      return true;
    }
  }
  return false;
}
