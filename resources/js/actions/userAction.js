import axios   from 'axios';
import store   from './../routes/store';
import history from "../routes/history";

let {dispatch} = store;

export async function storeUpdateData(url, data, type, skey = '') {

  const newUrl = type !== 'store' ? `/api/${url}/${skey}` : `/api/${url}`;

  const newData = new FormData();
  newData.append('name', data.name);
  newData.append('email', data.email);
  if (data.password !== "") {
    newData.append('password', data.password);
  }
  newData.append('address', data.address);
  newData.append('role_id', data.role_id);
  if (data.profile_pic) {
    newData.append('profile_pic', data.profile_pic);
  }
  newData.append('active', data.active ? 1 : 0);
  newData.append('_method', type !== 'store' ? 'PATCH' : 'POST');

  try {
    await axios.post(newUrl, newData);
    dispatch({
      type   : 'SNACKBAR_ALERT',
      payload: {
        open     : true,
        typeAlert: 'success',
        message  : `User ${type !== 'store' ? `updated` : 'created'} successfully`
      }
    });
    dispatch({type: 'RESET_GET_META_USERS'});
    history.push('/admin/user');
  } catch (e) {
    console.log('error');
  }
}
