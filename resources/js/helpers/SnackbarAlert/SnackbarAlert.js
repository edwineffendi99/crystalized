import React         from 'react';
import {Snackbar}    from '@material-ui/core';
import MuiAlert      from '@material-ui/lab/Alert';
import {useSelector} from "react-redux";
import store         from "../../routes/store";

const {dispatch} = store;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function SnackbarAlert() {
  const {snackbarAlert}            = useSelector(state => state.globalReducer);
  const {open, typeAlert, message} = snackbarAlert;

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    dispatch({type: 'SNACKBAR_ALERT', payload: {...snackbarAlert, open: false}});
  };

  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}
              anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
      <Alert onClose={handleClose} severity={typeAlert}>
        {message}
      </Alert>
    </Snackbar>
  );
}
