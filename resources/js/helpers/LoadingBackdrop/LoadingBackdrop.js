import React                                    from "react";
import {Backdrop, CircularProgress, makeStyles} from '@material-ui/core';
import {useSelector}                            from "react-redux";

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 200,
    color : '#fff',
  }
}));

export default function LoadingBackdrop() {
  let classes           = useStyles();
  let {loadingBackdrop} = useSelector(state => state.globalReducer);

  return (
    <Backdrop className={classes.backdrop} open={Boolean(loadingBackdrop)}>
      <CircularProgress color="inherit"/>
    </Backdrop>
  )
}
