import React                                from "react";
import clsx                                 from 'clsx';
import {CircularProgress, Fade, makeStyles} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root  : {
    height        : '100vh',
    flexDirection : 'column',
    justifyContent: 'center',
    alignItems    : 'center',
    display       : 'flex',
    '& > * + *'   : {
      marginLeft: theme.spacing(2),
    },
  },
  bgDark: {
    backgroundColor: '#1f1f1f !important'
  },
  logo  : {
    display       : 'flex',
    justifyContent: 'center',
    alignItems    : 'center',
    marginTop     : theme.spacing(2),
    marginBottom  : theme.spacing(2)
  },
}));

export default function LoadingPage() {
  let theme   = JSON.parse(localStorage.getItem('theme'));
  let classes = useStyles();
  let logoSrc = theme ? `/images/logo-light.png` : `/images/logo-dark.png`;

  return (
    <Fade in={true} timeout={150}>
      <div className={clsx(classes.root, !theme && classes.bgDark)}>
        <div className={classes.logo}>
          <img src={logoSrc} width={300} alt="logo"/>
        </div>
        <CircularProgress style={{color: theme ? '#1f1f1f' : '#fff'}}/>
      </div>
    </Fade>
  )
}
