import React         from 'react';
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  makeStyles
}                    from '@material-ui/core';
import {useSelector} from 'react-redux';
import store         from "../../routes/store";

const {dispatch} = store;

const useStyles = makeStyles(theme => ({
    wrapper       : {
      margin  : theme.spacing(1),
      position: 'relative',
    },
    buttonProgress: {
      position  : 'absolute',
      top       : '50%',
      left      : '50%',
      marginTop : -12,
      marginLeft: -12,
    },
  })
);

export default function AlertDialog() {
  const classes                             = useStyles();
  const {alertDialog, loading}              = useSelector(state => state.globalReducer);
  const {open, title, content, handleAgree} = alertDialog;

  const handleClose = () => {
    dispatch({type: 'ALERT_DIALOG', payload: {...alertDialog, open: false}});
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      {
        title &&
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      }
      <DialogContent style={{minWidth: 200}}>
        <DialogContentText id="alert-dialog-description">
          {content}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">Cancel</Button>
        {
          handleAgree &&
          <div className={classes.wrapper}>
            <Button
              variant="contained"
              color="primary"
              disabled={loading}
              onClick={handleAgree}
            >
              Confirm
            </Button>
            {loading && <CircularProgress size={24} className={classes.buttonProgress}/>}
          </div>
        }
      </DialogActions>
    </Dialog>
  );
}
