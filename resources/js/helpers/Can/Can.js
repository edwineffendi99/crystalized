import React from 'react';
import {useSelector} from "react-redux";

export default function Can({type, children}) {
  const {permissions} = useSelector(state => state.globalReducer);

  if (permissions.some(e => e === type)) {
    return children;
  }

  return null;
}
