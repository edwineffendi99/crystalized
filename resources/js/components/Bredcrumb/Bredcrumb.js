import React        from 'react';
import {makeStyles, Breadcrumbs, Typography, Link, Paper} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    padding     : theme.spacing(1, 3),
    marginBottom: theme.spacing(3)
  },
}));

export default function CollapsedBreadcrumbs(props) {
  const classes  = useStyles();
  const dataLink = props.location.pathname.split('/');
  const newLink  = [];
  const lastid   = dataLink.slice(-1)[0];

  dataLink.map((e, i, arr) => {
    if (i !== 0) {
      const urlPath = arr.slice(0, i + 1).join('/');
      if (e === lastid) {
        newLink.push({path: urlPath, name: e, active: true});
      } else {
        newLink.push({path: urlPath, name: e, active: false});
      }
    }
  });

  return (
    <Paper className={classes.root} elevation={0}>
      <Breadcrumbs maxItems={5} aria-label="breadcrumb">
        {
          newLink.map((e, i) => {
            return !e.active ? (
                <Link key={i} color="inherit" href={e.path}>
                  {e.name}
                </Link>
              )
              :
              (<Typography key={i} color="textPrimary">{e.name}</Typography>)
          })
        }
      </Breadcrumbs>
    </Paper>
  );
}
