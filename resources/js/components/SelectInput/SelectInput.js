import React                                                                   from 'react';
import {FormControl, FormHelperText, InputLabel, makeStyles, MenuItem, Select} from "@material-ui/core";
import {errorOn, messageError}                                                 from "../../actions/globalAction";

const useStyles = makeStyles(theme => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
}));

const SelectInput = ({value, disabled, onChange, name, menuItem, label, className, fullWidth = true}) => {
  const classes                     = useStyles();
  const inputLabel                  = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  const classNames                  = className ? className : classes.textField;

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  return (
    <FormControl variant="outlined" margin="dense" error={errorOn(name)} className={classNames} fullWidth={fullWidth} disabled={disabled}>
      <InputLabel ref={inputLabel} id="demo-simple-select-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        name={name}
        value={value}
        onChange={onChange}
        labelWidth={labelWidth}
      >
        {
          menuItem.map((item, key) => <MenuItem key={key} value={item.value}>{item.label}</MenuItem>)
        }
      </Select>
      {
        errorOn(name) &&
        <FormHelperText>{messageError(name)}</FormHelperText>
      }
    </FormControl>
  )
};

export default SelectInput
