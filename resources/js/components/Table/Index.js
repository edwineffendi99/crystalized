import React                      from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory}               from "react-router-dom";
import {deleteData}               from "../../actions/globalAction";

function Index({headCells, pathUrl, url}) {
  const {selected, page, rowsPerPage, order, orderBy} = useSelector(state => state.globalReducer);
  const {rows, meta} = useSelector(state => state.userReducer);
  const history                                                   = useHistory();
  const dispatch                                                  = useDispatch();

  const contentTableCell = (row, item) => {
    switch (item.id) {
      case "active":
        return row[item.id] ? 'active' : 'inactive';
      default :
        return row[item.id];
    }
  };

  const handleAction = (type, rowItem) => {
    switch (type) {
      case "show":
        history.push('');
        console.log('show');
        break;
      case "edit":
        history.push(`${pathUrl}/update/${rowItem.skey}`);
        break;
      case "delete":
        dispatch({
          type   : 'ALERT_DIALOG',
          payload: {
            open       : true,
            content    : 'Are you sure you want delete this?',
            handleAgree: () => deleteData(url, rowItem.skey)
          }
        });
        break;
    }
  };

  return (
    <table className="table">
      <thead>
      <tr>
        <th>
          <button className="btn-icon">
            <ion-icon name="square-outline" size="small"/>
          </button>
        </th>
        <th>No</th>
        {
          headCells.map(item => <th key={item.id}>{item.label}</th>)
        }
        <th/>
      </tr>
      </thead>
      <tbody>
      {
        rows.length ?
          rows.map((row, index) => {
              const noItem = page ? index + 1 + page * rowsPerPage : index + 1;
              return (
                <tr key={index}>
                  <td>
                    <button className="btn-icon">
                      <ion-icon name="square-outline" size="small"/>
                    </button>
                  </td>
                  <td>{noItem}</td>
                  {
                    headCells.map(item =>
                      <td key={item.id}>{contentTableCell(row, item)}</td>
                    )
                  }
                  <td className="text-right">
                    <button className="btn-icon" onClick={() => handleAction('show', row)}>
                      <ion-icon name="eye-sharp" size="small"/>
                    </button>
                    <button className="btn-icon" onClick={() => handleAction('edit', row)}>
                      <ion-icon name="pencil-sharp" size="small"/>
                    </button>
                    <button className="btn-icon" onClick={() => handleAction('delete', row)}>
                      <ion-icon name="trash-sharp" size="small"/>
                    </button>
                  </td>
                </tr>
              )
            }
          )
          :
          <tr>
            <td colSpan={3 + headCells.length} style={{textAlign: 'center'}}>Loading</td>
          </tr>
      }
      </tbody>
    </table>
  );
}

export default Index;
