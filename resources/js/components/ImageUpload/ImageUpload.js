import React, {useRef}              from 'react';
import clsx                         from 'clsx';
import {FormHelperText, makeStyles} from '@material-ui/core';
import PhotoCameraIcon              from '@material-ui/icons/PhotoCamera';
import {errorOn, messageError}      from "../../actions/globalAction";

const useStyles = makeStyles(theme => ({
  root        : {
    cursor: 'pointer'
  },
  button      : {
    margin: theme.spacing(1),
  },
  hide        : {
    display: 'none'
  },
  rootImage   : {
    marginBottom: theme.spacing(2)
  },
  center      : {
    textAlign: 'center'
  },
  wrapperImage: {
    width          : 160,
    height         : 160,
    borderRadius   : '100%',
    overflow       : 'hidden',
    margin         : '8px auto 0 auto',
    backgroundColor: '#F4F6F8',
    position       : 'relative'
  },
  image       : {
    width    : '100%',
    height   : '100%',
    objectFit: 'cover'
  },
  iconCamera  : {
    position       : 'absolute',
    width          : '100%',
    display        : 'flex',
    alignItems     : 'center',
    justifyContent : 'center',
    height         : '40px',
    bottom         : 0,
    backgroundColor: 'rgba(0,0,0,.6)',
  },
  photoCamera : {
    color: '#f3f3f3'
  }
}));

function ImageUpload(props) {
  const {onChange, name, preview, disabled} = props;
  const classes                             = useStyles();
  const inputElement                        = useRef(null);
  const disabledClass                       = clsx({
    [classes.root]: !disabled
  });

  const handleClickImageUpload = () => {
    inputElement.current.click();
  };

  return (
    <div className={disabledClass}>
      <div className={classes.rootImage}>
        {
          disabled ?
            <div className={classes.wrapperImage}>
              <img alt="upload-image" src={preview} className={classes.image}/>
            </div>
            :
            <div className={classes.wrapperImage} onClick={handleClickImageUpload}>
              <img alt="upload-image" src={preview} className={classes.image}/>
              <div className={classes.iconCamera}>
                <PhotoCameraIcon className={classes.photoCamera}/>
              </div>
            </div>
        }
        {
          errorOn(name) &&
          <FormHelperText error={errorOn(name)} className={classes.center}
                          variant="outlined">{messageError(name)}</FormHelperText>
        }
      </div>
      <input name={name}
             className={classes.hide}
             ref={inputElement}
             onChange={onChange}
             type="file" accept="image/*" capture/>
    </div>
  )
}

export default ImageUpload;
