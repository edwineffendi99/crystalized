import React                   from 'react';
import {errorOn, messageError} from "../../actions/globalAction";
import {makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
}));

function TextInput(props) {
  const classes       = useStyles();
  const {label, name, required, disabled} = props;

  return <TextField id={name}
                    {...props}
                    label={label}
                    name={name}
                    autoComplete="off"
                    error={errorOn(name)}
                    className={classes.textField}
                    helperText={messageError(name)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    autoFocus/>
}

export default TextInput;
