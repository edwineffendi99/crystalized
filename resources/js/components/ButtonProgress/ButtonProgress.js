import React                                  from 'react';
import clsx from "clsx";
import {useSelector}                          from "react-redux";
import {Button, CircularProgress, makeStyles} from '@material-ui/core';
import {green}                                from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  display       : {
    display: 'inline-block',
  },
  wrapper       : {
    margin  : theme.spacing(3, 0, 2),
    position: 'relative',
  },
  buttonSuccess : {
    backgroundColor: green[500],
    '&:hover'      : {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color     : green[500],
    position  : 'absolute',
    top       : '50%',
    left      : '50%',
    marginTop : -12,
    marginLeft: -12,
  },
}));

function ButtonProgress(props) {
  const classes   = useStyles();
  const counter   = useSelector(state => state);
  const {loading} = counter.globalReducer;

  const buttonWrapper = clsx({
    [classes.display]: !props.fullWidth,
  }, classes.wrapper);

  return (
    <div className={buttonWrapper}>
      <Button
        {...props}
        variant="contained"
        color="primary"
        disabled={loading}
      >
        {props.children}
      </Button>
      {loading && <CircularProgress size={24} className={classes.buttonProgress}/>}
    </div>
  );
}

export default ButtonProgress;
