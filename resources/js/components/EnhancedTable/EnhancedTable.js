import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector}   from "react-redux";
import PropTypes                    from 'prop-types';

import {makeStyles, Paper, Table, TableContainer, TablePagination} from '@material-ui/core';

import EnhancedTableToolbar       from './components/EnhancedTableToolbar';
import EnhancedTableSearch        from './components/EnhancedTableSearch';
import EnhancedTableHead          from './components/EnhancedTableHead';
import EnhancedTableBody          from './components/EnhancedTableBody';
import {deleteMultiData, getData} from "../../actions/globalAction";

const useStyles = makeStyles(theme => ({
  root          : {
    width: '100%',
  },
  paper         : {
    width       : '100%',
    padding     : theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  table         : {
    // minWidth: '100%',
  },
  visuallyHidden: {
    border  : 0,
    clip    : 'rect(0 0 0 0)',
    height  : 1,
    margin  : -1,
    overflow: 'hidden',
    padding : 0,
    position: 'absolute',
    top     : 20,
    width   : 1,
  },
}));

function EnhancedTable(props) {
  const classes                                                  = useStyles();
  const {url, headCells, disableSearch, pathUrl, label, getType} = props;
  const dispatch                                                 = useDispatch();
  const counter                                                  = useSelector(state => state);
  const [queryString, setQueryString]                            = useState('');

  const {rows, meta}                  = counter[pathUrl + 'Reducer'];
  const [order, setOrder]             = useState('desc');
  const [orderBy, setOrderBy]         = useState('created_at');
  const [selected, setSelected]       = useState([]);
  const [page, setPage]               = useState(meta.current_page);
  const [rowsPerPage, setRowsPerPage] = useState(meta.per_page);

  useEffect(() => {
    getData(`/api/${url}/index/${rowsPerPage}/${orderBy}/${order}?page=${page + 1}`, getType);
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc      = orderBy === property && order === 'asc';
    const isOrderAsc = isAsc ? 'desc' : 'asc';
    const urlSort    = `/api/${url}/index/${rowsPerPage}/${property}/${isOrderAsc}?page=${page + 1}${queryString}`;
    setOrder(isOrderAsc);
    setOrderBy(property);
    getData(urlSort, getType);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.skey);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected     = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    const noPage = newPage + 1;
    getData(meta.path + '?page=' + noPage + queryString, getType);
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    const numberRowPage = parseInt(event.target.value, 10);
    getData(`/api/${url}/index/${numberRowPage}/${orderBy}/${order}`, getType);
    setRowsPerPage(numberRowPage);
    setPage(0);
  };

  const deleteMultiple = () => {
    dispatch({
      type   : 'ALERT_DIALOG',
      payload: {
        open       : true,
        content    : 'Are you sure you want delete this selected?',
        handleAgree: () => deleteMultiData(url, selected, getType, setSelected)
      }
    });
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>

        <EnhancedTableToolbar numSelected={selected.length}
                              pathUrl={pathUrl}
                              label={label}
                              deleteMultiple={deleteMultiple}/>

        <EnhancedTableSearch headCells={headCells}
                             getType={getType}
                             url={url}
                             rowsPerPage={rowsPerPage}
                             orderBy={orderBy}
                             dispatch={dispatch}
                             order={order}
                             setPage={setPage}
                             disableSearch={disableSearch}
                             qString={setQueryString}/>

        <TableContainer>
          <Table className={classes.table} aria-labelledby="tableTitle" aria-label="enhanced table">
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              headCells={headCells}
              dispatch={dispatch}
              onRequestSort={handleRequestSort}
              handleSelectAllClick={handleSelectAllClick}
              rows={rows}
              rowCount={rows.length}
            />
            <EnhancedTableBody
              classes={classes}
              headCells={headCells}
              rowsPerPage={rowsPerPage}
              getType={getType}
              page={page}
              url={url}
              meta={meta}
              rows={rows}
              pathUrl={pathUrl}
              selected={selected}
              handleClick={handleClick}
              dispatch={dispatch}
            />
          </Table>
        </TableContainer>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={meta.total}
          rowsPerPage={parseInt(rowsPerPage)}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />

      </Paper>
    </div>
  );
}

EnhancedTable.propTypes = {
  headCells: PropTypes.array.isRequired,
  url      : PropTypes.string.isRequired,
};

export default EnhancedTable
