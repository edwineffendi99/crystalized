import React, {useState} from "react";
import PropTypes         from "prop-types";

import {Button, FormControl, lighten, makeStyles, TextField} from "@material-ui/core";
import SearchIcon                                            from '@material-ui/icons/Search';
import Autocomplete                                          from '@material-ui/lab/Autocomplete';

import {getData}   from "../../../../actions/globalAction";
import SelectInput from '../../../../components/SelectInput';

const useStyles = makeStyles(theme => ({
  root          : {
    flexWrap    : 'wrap',
    marginBottom: theme.spacing(1),
  },
  flex          : {
    display   : 'flex',
    alignItems: 'center'
  },
  highlight     :
    theme.palette.type === 'light' ?
      {
        color          : theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color          : theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title         : {
    flex: '1 1 100%',
  },
  textField     : {
    marginLeft : theme.spacing(1),
    marginRight: theme.spacing(1),
    minWidth   : 100
  },
  button        : {
    marginTop   : 8,
    marginBottom: 4,
    marginLeft  : 8,
    marginRight : 8
  },
  endAdornment  : {
    top: 'auto',
  },
  clearIndicator: {
    padding: '4px !important'
  },
  popupIndicator: {
    padding: '4px !important'
  }
}));

const EnhancedTableSearch = props => {
  const classes                                                                                           = useStyles();
  const {headCells, qString, disableSearch, dispatch, getType, url, rowsPerPage, orderBy, order, setPage} = props;
  const inputLabel                                                                                        = React.useRef(null);
  const [labelWidth, setLabelWidth]                                                                       = React.useState(0);
  let [state, setState]                                                                                   = useState({});

  React.useEffect(() => {
    setLabelWidth(inputLabel.current && inputLabel.current.offsetWidth);
    let fields = {};
    let id;
    headCells.map(item => {
      id         = item.searchId ? item.searchId : item.id;
      fields[id] = item.hasOwnProperty('data') ? item.data[0] : "";
    });

    setState(fields);
  }, []);

  const handleSearch = (e) => {
    e.preventDefault();
    let txt = "";
    let arr = [];
    let x;
    let value;
    for (x in headCells) {
      const id = headCells[x].searchId ? headCells[x].searchId : headCells[x].id;
      if (headCells[x].search) {
        arr.push(headCells[x]);
        if (headCells[x].searchType === 'select') {
          value = state[id].value;
        } else {
          value = state[id];
        }
        txt += `${arr.length === 1 ? '?' : '&'}${id}=${value}`;
      }
    }

    const urls       = `/api/${url}/index/${rowsPerPage}/${orderBy}/${order}${txt}`;
    const trimmedTxt = txt.replace('?', '&');
    qString(trimmedTxt);
    setPage(0);
    getData(urls, getType);
  };

  const handleAll = () => {
    setState(prevState => ({...prevState}));
    getData(`/api/${url}/index/${rowsPerPage}/${orderBy}/${order}`, getType);
    qString('');
    setPage(0);
    dispatch({type: `RESET_META_${getType}`});
  };

  const onChange = (event) => {
    const {name, value} = event.target;
    setState(prevState => ({...prevState, [name]: value}));
  };

  const returnSearchType = (e, i) => {
    const id = e.searchId ? e.searchId : e.id;

    switch (e.searchType) {
      case "text":
        return (
          <TextField
            key={i}
            label={e.label}
            value={state[id]}
            name={id}
            onChange={onChange}
            className={classes.textField}
            margin="dense"
            variant="outlined"
            autoComplete="off"
          />
        );
      case "old-select" :
        return (
          <SelectInput
            key={i}
            onChange={onChange}
            name={id}
            className={classes.textField}
            value={state[id]}
            label={e.label}
            menuItem={e.data}
            fullWidth={false}
          />
        );
      case "select" :
        return (
          <FormControl key={i} className={classes.textField}>
            <Autocomplete
              id={id}
              name={id}
              options={e.data}
              getOptionLabel={option => option.label}
              getOptionSelected={(option, value) => option.value === value.value}
              value={state[id]}
              size="small"
              classes={{
                endAdornment  : classes.endAdornment,
                clearIndicator: classes.clearIndicator,
                popupIndicator: classes.popupIndicator
              }}
              onChange={(event, value) => {
                setState(prevState => ({...prevState, [id]: value}));
              }}
              renderInput={params => <TextField {...params} label={e.label} margin="dense" variant="outlined"
                                                style={{width: 191}}/>}
            />
          </FormControl>
        )
    }
  };

  return !disableSearch && Object.keys(state).length && (
    <div className={classes.root}>
      <form onSubmit={(e) => handleSearch(e)}>
        {
          headCells.map((e, i) =>
            e.search && returnSearchType(e, i)
          )
        }
        <Button
          variant="contained"
          color="primary"
          size="medium"
          className={classes.button}
          onClick={(e) => handleSearch(e)}
          startIcon={<SearchIcon/>}
        >
          Search
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="medium"
          className={classes.button}
          onClick={(e) => handleAll(e)}
          startIcon={<SearchIcon/>}
        >
          All
        </Button>
      </form>
    </div>
  );
};

EnhancedTableSearch.propTypes = {
  headCells: PropTypes.array.isRequired,
};

export default EnhancedTableSearch;
