import React                                                           from "react";
import PropTypes                                                       from "prop-types";
import clsx                                                            from "clsx";
import {IconButton, lighten, makeStyles, Toolbar, Tooltip, Typography} from "@material-ui/core";
import DeleteIcon                                                      from '@material-ui/icons/Delete';
import FilterListIcon                                                  from '@material-ui/icons/FilterList';
import AddIcon                                                         from '@material-ui/icons/Add';
import {useHistory}                                                    from "react-router-dom";
import Can                                                             from "../../../../helpers/Can/Can";

const useToolbarStyles = makeStyles(theme => ({
  root     : {
    paddingLeft : theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  flex     : {
    display: 'flex'
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color          : theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color          : theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title    : {
    flex: '1 1 100%',
  },
}));

const EnhancedTableToolbar = props => {
  const history                                       = useHistory();
  const classes                                       = useToolbarStyles();
  const {numSelected, deleteMultiple, pathUrl, label} = props;

  const handleCreate = () => {
    history.push(`${pathUrl}/store`);
  };

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography className={classes.title} color="inherit" variant="subtitle1">
          {numSelected} selected
        </Typography>
      ) : (
        <Typography className={classes.title} variant="h4" id="tableTitle">
          {label}
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete" onClick={deleteMultiple}>
            <DeleteIcon/>
          </IconButton>
        </Tooltip>
      ) : (
        <div className={classes.flex}>
          <Tooltip title="Filter list">
            <IconButton aria-label="filter list">
              <FilterListIcon/>
            </IconButton>
          </Tooltip>
          <Can type={`${pathUrl}:add`}>
            <Tooltip title="Create">
              <IconButton aria-label="create" onClick={handleCreate}>
                <AddIcon/>
              </IconButton>
            </Tooltip>
          </Can>
        </div>
      )}
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected   : PropTypes.number.isRequired,
  deleteMultiple: PropTypes.func.isRequired,
};

export default EnhancedTableToolbar;
