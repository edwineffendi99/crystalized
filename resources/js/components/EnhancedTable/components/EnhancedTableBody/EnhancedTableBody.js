import React        from "react";
import {useHistory} from "react-router-dom";
import _            from 'lodash';

import {Checkbox, Chip, IconButton, TableBody, TableCell, TableRow, Tooltip, withStyles} from "@material-ui/core";

import Skeleton         from "@material-ui/lab/Skeleton/Skeleton";
import DeleteIcon       from '@material-ui/icons/Delete';
import EditIcon         from '@material-ui/icons/Edit';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';

import Can from "../../../../helpers/Can";

import {deleteData}  from "../../../../actions/globalAction";
import {useSelector} from "react-redux";

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(even)': {
      // backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function EnhancedTableBody(props) {
  const {
          headCells,
          url,
          rowsPerPage,
          page,
          meta,
          rows,
          selected,
          dispatch,
          pathUrl,
          handleClick,
          getType
        }             = props;
  const history       = useHistory();
  const {loading}     = useSelector(state => state.globalReducer);
  const isSelected    = name => selected.indexOf(name) !== -1;
  const emptyRows     = rowsPerPage - Math.min(rowsPerPage, rows.length);
  const countSkeleton = Math.min(rowsPerPage, meta.total - page * rowsPerPage);
  const dataSkeleton  = meta.current_page > 0 ? countSkeleton : countSkeleton > 0 ? countSkeleton : rowsPerPage;

  const contentTableCell = (row, item) => {
    const rowItem = _.get(row, item.id);

    switch (item.id) {
      case "active":
        return rowItem ? <Chip size="small" color="primary" label="Active"/> :
          <Chip size="small" color="secondary" label="Inactive"/>;
      default :
        return item.permalink ?
          <a href={_.get(row, item.permalink)} target="_blank"><Chip size="small" label={rowItem}
                                                                     style={{cursor: "pointer"}}/></a> : rowItem;
    }
  };

  const handleAction = (type, rowItem) => {
    switch (type) {
      case "detail":
        history.push(`${pathUrl}/show/${rowItem.skey}`);
        break;
      case "edit":
        history.push(`${pathUrl}/update/${rowItem.skey}`);
        break;
      case "delete":
        dispatch({
          type   : 'ALERT_DIALOG',
          payload: {
            open       : true,
            content    : 'Are you sure you want delete this?',
            handleAgree: () => deleteData(url, rowItem.skey, getType)
          }
        });
        break;
    }
  };

  const tableCellAction = (row) => {
    return (
      <TableCell padding="checkbox">
        <Can type={`${pathUrl}:read`}>
          <Tooltip title="Detail">
            <span>
              <IconButton aria-label="detail" disabled={!row} onClick={() => handleAction('detail', row)}>
                <RemoveRedEyeIcon/>
              </IconButton>
            </span>
          </Tooltip>
        </Can>
        <Can type={`${pathUrl}:edit`}>
          <Tooltip title="Edit">
            <span>
              <IconButton aria-label="edit" disabled={!row} onClick={() => handleAction('edit', row)}>
                <EditIcon/>
              </IconButton>
            </span>
          </Tooltip>
        </Can>
        <Can type={`${pathUrl}:delete`}>
          <Tooltip title="Delete">
            <span>
              <IconButton aria-label="delete" disabled={!row} onClick={() => handleAction('delete', row)}>
                <DeleteIcon/>
              </IconButton>
            </span>
          </Tooltip>
        </Can>
      </TableCell>
    )
  };

  const tableSkeleton = () => {
    let arr = [];
    let i;
    for (i = 0; i < dataSkeleton; i++) {
      arr.push(
        <StyledTableRow key={i}>
          <TableCell padding="checkbox">
            <Checkbox/>
          </TableCell>
          <TableCell>
            {page ? i + 1 + page * rowsPerPage : i + 1}
          </TableCell>
          {
            headCells.map((item, key) =>
              <TableCell key={key}>
                <Skeleton/>
              </TableCell>
            )
          }
          {
            tableCellAction()
          }
        </StyledTableRow>
      );
    }

    return arr;
  };

  return (
    <TableBody>
      {
        !loading ?
          rows.length !== 0 ?
            rows.map((row, index) => {
              const isItemSelected = isSelected(row.skey);
              const labelId        = `enhanced-table-checkbox-${index}`;
              const noItem         = page ? (index + 1) + (page * rowsPerPage) : (index + 1);

              return (
                <StyledTableRow
                  hover
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={row.skey}
                  selected={isItemSelected}>
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={isItemSelected}
                      onClick={(e) => handleClick(e, row.skey)}
                      inputProps={{'aria-labelledby': labelId}}
                    />
                  </TableCell>
                  <TableCell>
                    {noItem}
                  </TableCell>
                  {
                    headCells.map((item, i) =>
                      <TableCell key={i} padding={item.disablePadding ? 'none' : 'default'}>
                        {contentTableCell(row, item)}
                      </TableCell>
                    )
                  }
                  {
                    tableCellAction(row)
                  }
                </StyledTableRow>
              );
            })
            :
            <React.Fragment>
              <TableRow style={{height: 49}}>
                <TableCell colSpan={3 + headCells.length} align="center">No Data Records</TableCell>
              </TableRow>
              <TableRow style={{height: 49 * (rowsPerPage - 1)}}>
                <TableCell colSpan={3 + headCells.length}/>
              </TableRow>
            </React.Fragment>
          :
          tableSkeleton()
      }

      {
        rows.length ?
          (emptyRows > 0) && (
            <TableRow style={{height: 49 * emptyRows}}>
              <TableCell colSpan={3 + headCells.length}/>
            </TableRow>
          )
          :
          (dataSkeleton < emptyRows > 0) && (
            <TableRow style={{height: 49 * (emptyRows - dataSkeleton)}}>
              <TableCell colSpan={3 + headCells.length}/>
            </TableRow>
          )
      }

    </TableBody>
  )
}

export default EnhancedTableBody;
