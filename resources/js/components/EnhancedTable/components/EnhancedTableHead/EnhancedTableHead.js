import React                                                      from "react";
import PropTypes                                                  from "prop-types";
import {Checkbox, TableCell, TableHead, TableRow, TableSortLabel} from "@material-ui/core";

function EnhancedTableHead(props) {
  const {classes, order, orderBy, numSelected, rowCount, onRequestSort, headCells, handleSelectAllClick} = props;
  const createSortHandler                                                                                = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={handleSelectAllClick}
            inputProps={{'aria-label': 'select all desserts'}}
          />
        </TableCell>
        <TableCell padding="checkbox">
          No
        </TableCell>
        {
          headCells.map(headCell => {
              const id = headCell.idSearch ? headCell.idSearch : headCell.id;
              return (
                <TableCell
                  key={id}
                  align={headCell.numeric ? 'right' : 'left'}
                  padding={headCell.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === id ? order : false}>
                  <TableSortLabel
                    active={orderBy === id}
                    direction={orderBy === id ? order : 'asc'}
                    onClick={createSortHandler(id)}>
                    {headCell.label}
                    {orderBy === id ? (
                      <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
                    ) : null}
                  </TableSortLabel>
                </TableCell>
              )
            }
          )
        }
        <TableCell padding="default">
          Action
        </TableCell>
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes             : PropTypes.object.isRequired,
  headCells           : PropTypes.array.isRequired,
  numSelected         : PropTypes.number.isRequired,
  onRequestSort       : PropTypes.func.isRequired,
  handleSelectAllClick: PropTypes.func.isRequired,
  order               : PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy             : PropTypes.string.isRequired,
  rowCount            : PropTypes.number.isRequired,
};

export default EnhancedTableHead
