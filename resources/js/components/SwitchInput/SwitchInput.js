import React                                  from "react";
import {FormControlLabel, makeStyles, Switch} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
}));

const SwitchInput = ({name, value, checked, label, onChange, disabled}) => {
  const classes = useStyles();
  return (
    <FormControlLabel
      className={classes.textField}
      disabled={disabled}
      control={
        <Switch
          onChange={onChange}
          name={name}
          value={value}
          checked={checked}
          color="primary"
        />
      }
      label={label}
    />
  )
};

export default SwitchInput;
