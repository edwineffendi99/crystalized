import React, {Component} from 'react';
import {ThemeProvider}    from '@material-ui/styles';
import {Provider}         from 'react-redux';
import store              from './routes/store';
import theme              from './theme';
import './assets/less/style.css';
import './routes/interceptor';
import IndexRoute         from './routes/index';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <IndexRoute/>
        </ThemeProvider>
      </Provider>
    );
  }
}
