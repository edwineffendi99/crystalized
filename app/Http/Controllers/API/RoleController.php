<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Role\AllRequest;
use App\Http\Requests\API\Role\DeleteMultiRequest;
use App\Http\Requests\API\Role\DeleteRequest;
use App\Http\Requests\API\Role\FindRequest;
use App\Http\Requests\API\Role\IndexRequest;
use App\Http\Requests\API\Role\PatchRequest;
use App\Http\Requests\API\Role\PutRequest;
use App\Http\Resources\API\Role\RolePagination;
use App\Services\API\RoleService;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @group  Roles
 *
 * APIs for roles management
 */
class RoleController extends Controller {

  private object $roleService;

  public function __construct(RoleService $roleService) {
    $this->roleService = $roleService;
  }

  /**
   * Paginated
   * Get paginated roles
   * @urlParam perPage records per page, default:20. Example: 3
   * @urlParam orderBy order by field, default:created_at. Example: created_at
   * @urlParam sortOrder order sort, default:DESC. Example: DESC
   *
   * @param IndexRequest $request
   * @param int $perPage
   * @param string $orderBy
   * @param string $orderSort
   * @return RolePagination|JsonResponse
   * @throws Exception
   */
  public function index(IndexRequest $request, $perPage = 20, $orderBy = 'created_at', $orderSort = 'DESC') {
    try {
      return $this->roleService->__index($request->validated(), $perPage, $orderBy, $orderSort);
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * All
   * Get all roles
   *
   * @param AllRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function all(AllRequest $request) {
    try {
      return responseSuccess($this->roleService->__all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Store
   * Store role data
   * @bodyParam name string required The name of the role. Example: administrator
   * @bodyParam active boolean required The state of role. Example: 1
   * @bodyParam permissions_id array required The array of permissions id
   * @bodyParam permissions_id.* int required The permissions id. Example: 1
   *
   * @param PutRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function store(PutRequest $request) {
    try {
      $this->roleService->__store($request->validated());
      return responseSuccess([], 'Role created successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Find
   * Find role
   * @urlParam skey required The role skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d037
   *
   * @param FindRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function find(FindRequest $request, $skey = '') {
    try {
      return responseSuccess($this->roleService->__find($skey));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Update
   * Update role
   * @urlParam skey required The role skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d037
   * @bodyParam name string The name of the role. Example: administrator
   * @bodyParam active boolean The state of role. Example: 1
   * @bodyParam permissions_id array The array of permissions id
   * @bodyParam permissions_id.* int The permissions id. Example: 1
   *
   * @param PatchRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function update(PatchRequest $request, $skey = '') {
    try {
      $this->roleService->__update($request->validated(), $skey);
      return responseSuccess([], 'Role updated successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Delete
   * Delete single role
   * @urlParam skey required The user skey. Example:13013eca-47d2-4bcb-b365-14d605f3d038
   *
   * @param DeleteRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function delete(DeleteRequest $request, $skey = '') {
    try {
      $this->roleService->__delete($skey);
      return responseSuccess([], 'Role deleted successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Delete multiple
   * Delete multiple roles
   * @urlParam skey required The user skey. Example:13013eca-47d2-4bcb-b365-14d605f3d038
   * @bodyParam skeys array required The skeys array of the user.
   * @bodyParam skeys.* string required single skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d038
   *
   * @param DeleteMultiRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function deleteMulti(DeleteMultiRequest $request) {
    try {
      $this->roleService->__deleteMulti($request->validated());
      return responseSuccess([], 'Role(s) deleted successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}
