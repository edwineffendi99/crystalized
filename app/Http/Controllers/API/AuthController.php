<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Auth\PostRequest;
use App\Http\Requests\API\Auth\RefreshRequest;
use App\Services\API\AuthService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @group  Authentication
 *
 * APIs for authentication
 */
class AuthController extends Controller {

  private object $authService;

  public function __construct(AuthService $authService) {
    $this->authService = $authService;
  }

  /**
   * Login
   * Authenticate to the API
   * @bodyParam  email string required The email of the user. Example: admin@mail.com
   * @bodyParam  password string required The password of the user. Example: password
   *
   * @param PostRequest $request
   * @param ServerRequestInterface $serverRequest
   * @return JsonResponse
   * @throws Exception
   */
  public function login(PostRequest $request, ServerRequestInterface $serverRequest) {
    try {
      $auth = $this->authService->__authenticate($request->validated(), $serverRequest);
      return responseSuccess($auth)
        ->withCookie(
          'at',
          $auth->access_token,
          43200,
          null,
          null,
          false,
          true)
        ->withCookie('rt',
          $auth->refresh_token,
          43200,
          null,
          null,
          false,
          true);
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Profile
   * Get logged profile
   * @authenticated
   * @return JsonResponse
   * @throws Exception
   */
  public function profile() {
    try {
      $profile = $this->authService->__profile();
      return responseSuccess($profile);
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Refresh token
   * Refresh the expired token
   * @bodyParam  refresh_token string required Previously refresh token. Example: def50200c6c70cbaa026b3da87604695174de0
   * @response  {
   * "status_code": 200,
   * "error_code": 0,
   * "message": "Success",
   * "data": {
   * "access_token": "def50200c6c70cbaa026b3da87604695174de1",
   * "refresh_token": "def50200c6c70cbaa026b3da87604695174de2",
   * "token_type": "Bearer",
   * "expires_at": "02-02-2020 12:00:00"
   * }
   * }
   *
   * @param RefreshRequest $request
   * @param ServerRequestInterface $serverRequest
   * @return JsonResponse
   * @throws Exception
   */
  public function refreshToken(RefreshRequest $request, ServerRequestInterface $serverRequest) {
    try {
      $auth = $this->authService->__refreshToken($serverRequest, $request->validated());
      return responseSuccess($auth)
        ->withCookie(
          'at',
          $auth->access_token,
          43200,
          null,
          null,
          false,
          true)
        ->withCookie(
          'rt',
          $auth->refresh_token,
          43200,
          null,
          null,
          false,
          true);
    } catch (Exception $exception) {
      Cookie::forget('at');
      Cookie::forget('rt');
      throw $exception;
      /*return responseError([], $exception->getMessage(), $exception->getCode())
        ->withCookie()
        ->withCookie(Cookie::forget('rt'));*/
    }
  }

  /**
   * Logout
   * Logout from API
   * @authenticated
   * @return JsonResponse
   * @throws Exception
   */
  public function logout() {
    try {
      if (Auth::guard('user_api')->check()) {
        Auth::guard('user_api')->user()->token()->revoke();
      }
      return responseSuccess([], 'Logout success')
        ->withCookie(Cookie::forget('at'))
        ->withCookie(Cookie::forget('rt'));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}
