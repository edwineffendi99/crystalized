<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\User\AllRequest;
use App\Http\Requests\API\User\DeleteMultiRequest;
use App\Http\Requests\API\User\DeleteRequest;
use App\Http\Requests\API\User\FetchRequest;
use App\Http\Requests\API\User\FindRequest;
use App\Http\Requests\API\User\IndexRequest;
use App\Http\Requests\API\User\PatchRequest;
use App\Http\Requests\API\User\PutRequest;
use App\Http\Resources\API\User\UserPagination;
use App\Services\API\UserService;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @group  Users
 *
 * APIs for users management
 */
class UserController extends Controller {

  private object $userService;

  public function __construct(UserService $userService) {
    $this->userService = $userService;
  }

  /**
   * Paginated
   * Get paginated users
   * @urlParam perPage records per page, default:20. Example: 3
   * @urlParam orderBy order by field, default:created_at. Example: created_at
   * @urlParam sortOrder order sort, default:DESC. Example: DESC
   *
   * @param IndexRequest $request
   * @param int $perPage
   * @param string $orderBy
   * @param string $orderSort
   * @return UserPagination|JsonResponse
   * @throws Exception
   */
  public function index(IndexRequest $request, $perPage = 20, $orderBy = 'created_at', $orderSort = 'DESC') {
    try {
      return $this->userService->__index($request->validated(), $perPage, $orderBy, $orderSort);
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * All
   * Get all users
   *
   * @param AllRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function all(AllRequest $request) {
    try {
      return responseSuccess($this->userService->__all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Fetch
   * Fetch user by name
   * @queryParam name required The name of a user to be fetch. Example:admin
   *
   * @param FetchRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function fetch(FetchRequest $request) {
    try {
      return responseSuccess($this->userService->__fetch($request->validated()));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Store
   * Store user data
   * @bodyParam name string required The name of the user.
   * @bodyParam email string required The email of the user. Example: user@mail.com
   * @bodyParam password string required The password of the user.
   * @bodyParam address string required The address of the user
   * @bodyParam role_id uuid required The role skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d037
   * @bodyParam profile_pic image required The profile pic of user
   * @bodyParam active boolean required The active of user
   * @response  {
   * "status_code": 200,
   * "error_code": 0,
   * "message": "Success",
   * "data": null
   * }
   * @param PutRequest $request
   * @return JsonResponse
   * @throws Exception
   */
  public function store(PutRequest $request) {
    try {
      $this->userService->__store($request->validated());
      return responseSuccess([], 'User created successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Find
   * Find user by skey
   * @urlParam skey required The user skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d037
   *
   * @param FindRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function find(FindRequest $request, $skey = '') {
    try {
      return responseSuccess($this->userService->__find($skey));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Update
   * Update user by skey
   * @urlParam skey required The user skey. Example:13013eca-47d2-4bcb-b365-14d605f3d037
   * @bodyParam name string The name of the user.
   * @bodyParam email string The email of the user. Example: user@mail.com
   * @bodyParam password string The password of the user.
   * @bodyParam address string The address of the user
   * @bodyParam role_id uuid The role skey. Example: 77635d38-8478-4162-aee8-2dad975c173b
   * @bodyParam profile_pic image The profile pic of user
   * @bodyParam active boolean The active of user
   * @response  {
   * "status_code": 200,
   * "error_code": 0,
   * "message": "Success",
   * "data": null
   * }
   * @param PatchRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function update(PatchRequest $request, $skey = '') {
    try {
      $this->userService->__update($request->validated(), $skey);
      return responseSuccess([], 'User updated successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Delete
   * Delete user by skey
   * @urlParam skey required The user skey. Example:13013eca-47d2-4bcb-b365-14d605f3d038
   *
   * @param DeleteRequest $request
   * @param string $skey
   * @return JsonResponse
   * @throws Exception
   */
  public function delete(DeleteRequest $request, $skey = '') {
    try {
      $this->userService->__delete($skey);
      return responseSuccess([], 'User deleted successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Delete multi
   * Delete multiple user by skey
   * @bodyParam skeys array required The skeys array of the user.
   * @bodyParam skeys.* string required single skey. Example: 13013eca-47d2-4bcb-b365-14d605f3d038
   *
   * @param DeleteMultiRequest $request
   * @throws Exception
   * @return JsonResponse
   */
  public function deleteMulti(DeleteMultiRequest $request) {
    try {
      $this->userService->__deleteMulti($request->validated());
      return responseSuccess([], 'User(s) deleted successfully');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}
