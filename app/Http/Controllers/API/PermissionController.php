<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\API\PermissionService;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @group  Permission
 *
 * APIs for permission
 */
class PermissionController extends Controller {

  private object $permissionService;

  public function __construct(PermissionService $permissionService) {
    $this->permissionService = $permissionService;
  }

  /**
   * All
   * Get list of permissions of current login user
   *
   * @return JsonResponse
   * @throws Exception
   */
  public function all() {
    try {
      return responseSuccess($this->permissionService->__all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}
