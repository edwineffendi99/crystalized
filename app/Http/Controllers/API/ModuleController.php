<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Role\IndexRequest;
use App\Services\API\ModuleService;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @group  Modules
 *
 * APIs for modules management
 */
class ModuleController extends Controller {

  private object $moduleService;

  public function __construct(ModuleService $moduleService) {
    $this->moduleService = $moduleService;
  }

  /**
   * All
   * Get all modules
   * @return JsonResponse
   * @throws Exception
   */
  public function all() {
    try {
      return responseSuccess($this->moduleService->__all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}
