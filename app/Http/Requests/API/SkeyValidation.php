<?php

namespace App\Http\Requests\API;

use Illuminate\Validation\ValidationException;

trait SkeyValidation {

  /**
   * @param $skey
   * @throws ValidationException
   */
  public function uuidValidate($skey) {
    $UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
    $valid  = preg_match($UUIDv4, $skey);
    if (!$valid) {
      throw ValidationException::withMessages([
        'skey' => ['the skey format is not recognized'],
      ]);
    }
  }

}