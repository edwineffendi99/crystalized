<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('user:browse');
  }

  public function validationData() {
    return array_merge($this->all(), [
      'name'       => $this->name,
      'email'      => $this->email,
      'address'    => $this->address,
      'role_id'    => $this->role_id,
      'active'     => $this->active,
      'start_date' => $this->start_date,
      'end_date'   => $this->end_date
    ]);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name'       => 'nullable',
      'email'      => 'nullable',
      'address'    => 'nullable',
      'role_id'    => 'nullable|exists:roles,skey',
      'active'     => 'nullable|boolean',
      'start_date' => 'nullable|required_with:end_date|date|beforeOrEqual:end_date',
      'end_date'   => 'nullable|required_with:start_date|date|afterOrEqual:start_date'
    ];
  }
}
