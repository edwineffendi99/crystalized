<?php

namespace App\Http\Requests\API\User;

use App\Http\Requests\API\SkeyValidation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\ValidationException;

class FindRequest extends FormRequest {

  use SkeyValidation;

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('user:read');
  }

  public function validationData() {
    return array_merge($this->all(), ['skey' => $this->route('skey')]);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   * @throws ValidationException
   */
  public function rules() {
    $skey = $this->route('skey');
    $this->uuidValidate($skey);
    return [
      'skey' => 'bail|required|uuid',
    ];
  }

}
