<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class PutRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('user:add');
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name'        => 'required',
      'email'       => 'required|email|unique:users,email',
      'password'    => 'required',
      'address'     => 'required',
      'role_id'     => 'required|exists:roles,skey',
      'profile_pic' => 'required|image|max:2048',
      'active'      => 'required|boolean',
    ];
  }
}
