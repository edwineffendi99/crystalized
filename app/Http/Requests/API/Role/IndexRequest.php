<?php

namespace App\Http\Requests\API\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('role:browse');
  }

  public function validationData() {
    return array_merge($this->all(), [
      'name'   => $this->name,
      'active' => $this->active
    ]);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name'   => 'nullable',
      'active' => 'nullable|boolean'
    ];
  }
}
