<?php

namespace App\Http\Requests\API\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class PutRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('role:add');
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'name'             => 'required|unique:roles,name',
      'active'           => 'required|boolean',
      'permissions_id'   => 'required|array',
      'permissions_id.*' => 'required|exists:permissions,id'
    ];
  }
}
