<?php

namespace App\Http\Requests\API\Role;

use App\Http\Requests\API\SkeyValidation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\ValidationException;

class PatchRequest extends FormRequest {

  use SkeyValidation;

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('role:edit');
  }

  public function validationData() {
    return array_merge($this->all(), ['skey' => $this->route('skey')]);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   * @throws ValidationException
   */
  public function rules() {
    $skey = $this->route('skey');
    $this->uuidValidate($skey);
    return [
      'skey'             => 'bail|required|uuid',
      'name'             => 'nullable',
      'active'           => 'nullable|boolean',
      'permissions_id'   => 'nullable|array',
      'permissions_id.*' => 'nullable|exists:permissions,id'
    ];
  }

}
