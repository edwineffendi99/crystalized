<?php

namespace App\Http\Requests\API\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class DeleteRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return Gate::allows('role:delete');
  }

  public function validationData() {
    return array_merge($this->all(), ['skey' => $this->route('skey')]);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'skey' => 'required|uuid',
    ];
  }
}
