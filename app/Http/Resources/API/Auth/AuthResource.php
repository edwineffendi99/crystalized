<?php

namespace App\Http\Resources\API\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'access_token'  => $this->access_token,
      'refresh_token' => $this->refresh_token,
      'token_type'    => $this->token_type,
      'expires_at'    => $this->additional['expires_at'],
    ];
  }
}
