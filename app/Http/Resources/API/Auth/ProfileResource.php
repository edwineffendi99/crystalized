<?php

namespace App\Http\Resources\API\Auth;

use App\Http\Resources\API\Role\RoleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'name'        => $this->name,
      'email'       => $this->email,
      'active'      => $this->active,
      'role'        => $this->when($this->relationLoaded('role'), function () {
        return new RoleResource($this->role);
      }),
      'profile_pic' => url('storage/' . $this->profile_pic),
    ];
  }
}
