<?php

namespace App\Http\Resources\API\Role;

use Illuminate\Http\Resources\Json\JsonResource;

class SimpleRoleResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {

    return [
      'skey' => $this->skey,
      'name' => $this->name,
    ];
  }
}
