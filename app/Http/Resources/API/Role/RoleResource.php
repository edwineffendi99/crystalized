<?php

namespace App\Http\Resources\API\Role;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'skey'        => $this->skey,
      'name'        => $this->name,
      'active'      => $this->active,
      'permalink'   => url('/admin/role/show/' . $this->skey),
      'created_at'  => $this->created_at->format('d-m-Y h:i:s'),
      'permissions' => $this->when($this->relationLoaded('permissions'), function () {
        return $this->permissions->pluck('id');
      })
    ];
  }
}
