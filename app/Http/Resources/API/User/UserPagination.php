<?php

namespace App\Http\Resources\API\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPagination extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'message'     => "Success",
      'status_code' => 200,
      'error_code'  => 0,
      'meta'        => [
        'per_page'          => $this->perPage(),
        'total'             => $this->total(),
        'current_page'      => $this->currentPage(),
        'last_page'         => $this->lastPage(),
        'last_page_url'     => $this->url($this->lastPage()),
        'next_page_url'     => $this->nextPageUrl(),
        'previous_page_url' => $this->previousPageUrl(),
        'first_page_url'    => $this->url(1),
        'from'              => $this->firstItem(),
        'to'                => $this->lastItem(),
        'path'              => $this->resource->toArray()['path']
      ],
      'data'        => UserResource::collection($this->items())
    ];
  }
}
