<?php

namespace App\Http\Resources\API\User;

use App\Http\Resources\API\Role\RoleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'skey'        => $this->skey,
      'name'        => $this->name,
      'email'       => $this->email,
      'address'     => $this->address,
      'role'        => new RoleResource($this->whenLoaded('role')),
      'profile_pic' => url('storage/' . $this->profile_pic),
      'active'      => $this->active
    ];
  }
}
