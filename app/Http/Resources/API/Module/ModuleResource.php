<?php

namespace App\Http\Resources\API\Module;

use App\Http\Resources\API\Role\PermissionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ModuleResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    return [
      'skey'        => $this->skey,
      'name'        => $this->name,
      'key'         => $this->key,
      'priority'    => $this->priority,
      'active'      => $this->active,
      'created_at'  => $this->created_at->format('d-m-Y h:i:s'),
      'permissions' => PermissionResource::collection($this->whenLoaded('permissions'))
    ];
  }
}
