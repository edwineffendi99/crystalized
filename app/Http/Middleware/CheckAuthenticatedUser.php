<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class CheckAuthenticatedUser {
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    $user = Auth::guard('user_api')->user();
    if (!$user->active) {
      $user->token()->revoke();
      return responseError([], 'Logging out now, unable to continue with status inactive', 403, 1)
        ->withCookie(Cookie::forget('at'))
        ->withCookie(Cookie::forget('rt'));
    }
    return $next($request);
  }
}
