<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class UserCookieAuth {

  public function handle($request, Closure $next) {
    if (Cookie::has('at')) {
      $request->headers->set('Authorization', 'Bearer ' . Cookie::get('at'));
    }

    return $next($request);
  }

}
