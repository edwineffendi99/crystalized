<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\UnauthorizedException;

class APICSRFProtection {
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   * @return mixed
   * @throws UnauthorizedException
   * @throws Exception
   */
  public function handle($request, Closure $next) {
    if (config('app.env') == 'local') {
      return $next($request);
    }

    if (Cookie::has('at')) {
      try {
        if (!$request->headers->has('x-xsrf-token')) {
          throw new UnauthorizedException('Request was rejected');
        }
        $xsrfHeader = Crypt::decrypt($request->header('x-xsrf-token'), false);
        if ($xsrfHeader !== Cookie::get('XSRF-TOKEN')) {
          throw new UnauthorizedException('Request was rejected');
        }
        return $next($request);
      } catch (Exception $exception) {
        throw $exception;
      }
    }
    return $next($request);
  }

}
