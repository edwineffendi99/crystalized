<?php

namespace App\Exceptions;

use Exception;

class CrystalizedException extends Exception {
  private $data;

  public function __construct($message, $code, array $data) {
    parent::__construct($message, $code);

    $this->data = $data;
  }

  public function getData() {
    return $this->data;
  }

}
