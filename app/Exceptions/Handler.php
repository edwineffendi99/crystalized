<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class Handler extends ExceptionHandler {
  /**
   * A list of the exception types that are not reported.
   *
   * @var array
   */
  protected $dontReport = [
    //
  ];

  /**
   * A list of the inputs that are never flashed for validation exceptions.
   *
   * @var array
   */
  protected $dontFlash = [
    'password',
    'password_confirmation',
  ];

  /**
   * Report or log an exception.
   *
   * @param \Exception $exception
   * @return void
   *
   * @throws \Exception
   */
  public function report(\Throwable $exception) {
    if (app()->bound('sentry') && $this->shouldReport($exception)) {
      app('sentry')->captureException($exception);
    }

    parent::report($exception);
  }

  /**
   * Render an exception into an HTTP response.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Throwable $exception
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Throwable
   */
  public function render($request, \Throwable $exception) {
    if ($exception instanceof ValidationException) {
      return $request->expectsJson() ?
        responseError($exception->validator->getMessageBag()->getMessages(), 'The given data was invalid', 422, 1)
        :
        parent::render($request, $exception);
    } else if ($exception instanceof AuthenticationException) {
      return $request->expectsJson() ?
        responseError([], $exception->getMessage() ? $exception->getMessage() : 'Unauthenticated', 401, 1)
        :
        parent::render($request, $exception);
    } else if ($exception instanceof AuthorizationException) {
      return $request->expectsJson() ?
        responseError([], $exception->getMessage(), 403, 1)
        :
        parent::render($request, $exception);
    } else if ($exception instanceof ModelNotFoundException) {
      return $request->expectsJson() ?
        responseError([], 'Entry for ' . str_replace('App\\', '', $exception->getModel()) . ' not found', 404, 1)
        :
        parent::render($request, $exception);
    } else if ($exception instanceof NotFoundHttpException) {
      return $request->expectsJson() ?
        responseError([], 'Route not found', 404, 1)
        :
        parent::render($request, $exception);
    } else if ($exception instanceof CrystalizedException) {
      return
        responseError($exception->getData(), $exception->getMessage(), $exception->getCode(), 1);
    } else {
      return $request->expectsJson() ?
        responseError([], $exception->getMessage(), 404, 1)
        :
        parent::render($request, $exception);
    }

    return parent::render($request, $exception);
  }
}
