<?php

namespace App\Providers;

use App\Services\API\HelperService;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider {
  /**
   * Register services.
   *
   * @return void
   */
  public function register() {
    $this->app->bind('Helper', function () {
      return new HelperService();
    });
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot() {
    //
  }
}
