<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use App\Traits\RolePermission;

class AuthServiceProvider extends ServiceProvider {
  use RolePermission;
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    // 'App\Model' => 'App\Policies\ModelPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot() {
    $this->registerPolicies();

    $this->defineGates();

    Passport::routes();

    Passport::tokensExpireIn(now()->addHours(2));

    Passport::refreshTokensExpireIn(now()->addMonth());

    Passport::personalAccessTokensExpireIn(now()->addHour());

    //snippet: passport scope definition
    /*Passport::tokensCan(Permission::all()->pluck('name', 'key')->toArray());*/
  }
}
