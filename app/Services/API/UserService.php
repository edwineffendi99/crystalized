<?php

namespace App\Services\API;

use App\Facades\Helper;
use App\Http\Resources\API\User\SimpleUserResource;
use App\Http\Resources\API\User\UserPagination;
use App\Http\Resources\API\User\UserResource;
use App\Models\Role;
use App\Models\User;
use App\Traits\RolePermission;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class UserService {

  use RolePermission;

  protected string $module = 'user';

  /**
   * Get paginated user
   * @param array $data
   * @param integer $perPage
   * @param string $orderBy
   * @param string $orderSort
   * @return UserPagination
   * @throws Exception
   */
  public function __index(array $data, $perPage, $orderBy, $orderSort) {
    try {
      return new UserPagination(
        User::with('role')->when($data['start_date'], function ($q) use ($data) {
          $startDate = Carbon::parse($data['start_date'])->startOfDay();
          $endDate   = Carbon::parse($data['end_date'])->endOfDay();
          return $q->whereBetween('created_at', [$startDate, $endDate]);
        })->when($data['name'], fn($q, $name) => $q->where('name', 'ilike', '%' . $name . '%'))
          ->when($data['email'], fn($q, $email) => $q->where('email', 'ilike', '%' . $email . '%'))
          ->when($data['role_id'], fn($q, $roleId) => $q->whereHas('role', function ($q) use ($roleId) {
            $q->where('skey', $roleId);
          }))
          ->when($data['address'], fn($q, $address) => $q->where('address', 'ilike', '%' . $address . '%'))
          ->when(isset($data['active']), fn($q, $active) => $q->where('active', $data['active']))
          ->orderBy($orderBy, $orderSort)
          ->paginate($perPage)
          ->appends($data));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Get all user
   * @throws Exception
   */
  public function __all() {
    try {
      return SimpleUserResource::collection(User::all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Fetch user
   * @param array $data
   * @return ResourceCollection
   * @throws Exception
   */
  public function __fetch($data) {
    try {
      return SimpleUserResource::collection(User::where(key($data), 'ilike', '%' . current($data) . '%')->get());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Store user
   * @param array $data
   * @throws Exception
   */
  public function __store(array $data) {
    try {
      DB::beginTransaction();
      $role     = Role::where('skey', $data['role_id'])->firstOrFail();
      $fileName = Helper::upload('users', $data['profile_pic']);
      User::create(array_merge($data, [
        'profile_pic' => $fileName,
        'role_id'     => $role->id
      ]));
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Find user
   * @param string $skey
   * @return UserResource
   * @throws Exception
   */
  public function __find(string $skey) {
    try {
      return new UserResource(User::with('role')->where('skey', $skey)->firstOrFail());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Update user
   * @param string $skey
   * @param array $data
   * @throws Exception
   */
  public function __update(array $data, string $skey) {
    try {
      DB::beginTransaction();
      $user = User::where('skey', $skey)->firstOrFail();
      if (array_key_exists('profile_pic', $data)) {
        $fileName = Helper::upload('users', $data['profile_pic']);
        $data     = array_merge($data, ['profile_pic' => $fileName]);
      }

      if (array_key_exists('role_id', $data)) {
        $role = Role::where('skey', $data['role_id'])->firstOrFail();
        $data = array_merge($data, ['role_id' => $role->id]);
      }
      $user->update($data);
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Delete user
   * @param string $skey
   * @throws Exception
   */
  public function __delete(string $skey) {
    try {
      DB::beginTransaction();
      $user = User::where('skey', $skey)->firstOrFail();
      $user->delete();
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Delete multiple user
   * @param array $skeys
   * @throws Exception
   */
  public function __deleteMulti(array $skeys) {
    try {
      DB::beginTransaction();
      $ids = User::whereIn('skey', $skeys['skeys'])->pluck('id')->toArray();
      User::destroy($ids);
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

}