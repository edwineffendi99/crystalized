<?php

namespace App\Services\API;

use App\Http\Resources\API\Auth\AuthResource;
use App\Http\Resources\API\Auth\ProfileResource;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Exceptions\OAuthServerException;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;

class AuthService extends AccessTokenController {

  public function __construct(AuthorizationServer $server,
                              TokenRepository $tokens,
                              Parser $jwt) {
    parent::__construct($server, $tokens, $jwt);
  }

  /**
   * Authenticate
   * @param array $data
   * @param ServerRequestInterface $serverRequest
   * @return AuthResource
   * @throws Exception
   */
  public function __authenticate(array $data, $serverRequest) {
    try {
      $passwordClient = DB::table('oauth_clients')->where('password_client', true)->first();
      if (empty($passwordClient)) {
        throw new AuthenticationException('Password grant has not been made');
      }

      try {
        //snippet: implementation passport scope
        /*$user        = User::with('role', 'role.permissions')->where('email', $data['email'])->firstOrFail();
        $permissions = $user->role->permissions->pluck('key')->toArray();
        $permissions = implode(' ', $permissions);*/
        $possibleUser = User::where('email', $data['email'])->firstOrFail();
      } catch (Exception $exception) {
        throw new AuthenticationException('Invalid credentials');
      }

      if(!$possibleUser->active){
        throw new Exception('Unable to login with status inactive');
      }

      $parsedBody                  = $serverRequest->getParsedBody();
      $parsedBody['username']      = $data['email'];
      $parsedBody['password']      = $data['password'];
      $parsedBody['grant_type']    = 'password';
      $parsedBody['client_id']     = $passwordClient->id;
      $parsedBody['client_secret'] = $passwordClient->secret;
      //$parsedBody['scope']         = $permissions;

      try {
        $response = parent::issueToken($serverRequest->withParsedBody($parsedBody));
      } catch (Exception $exception) {
        throw new AuthenticationException('Invalid credentials');
      }

      $token = json_decode($response->getContent());
      DB::commit();
      return (new AuthResource($token))->additional([
        'expires_at' => Carbon::now()->addSeconds($token->expires_in)->format('d-m-Y H:i:s'),
      ]);
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * logged profile
   * @return ProfileResource
   * @throws Exception
   */
  public function __profile() {
    try {
      if (Auth::guard('user_api')->check()) {
        return new ProfileResource(Auth::guard('user_api')->user());
      }

      throw new AuthenticationException();
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Refresh token
   * @param array $refreshTokenByRequest
   * @param ServerRequestInterface $serverRequest
   * @return AuthResource
   * @throws Exception
   */
  public function __refreshToken($serverRequest, $refreshTokenByRequest) {
    try {
      $refreshTokenByCookie = request()->hasCookie('rt');
      if (!$refreshTokenByCookie && empty($refreshTokenByRequest)) {
        throw new AuthenticationException('Please relogin to continue!');
      }

      $passwordClient = DB::table('oauth_clients')->where('password_client', true)->first();
      if (empty($passwordClient)) {
        throw new AuthenticationException('Password grant has not been made');
      }

      $parsedBody                  = $serverRequest->getParsedBody();
      $parsedBody['grant_type']    = 'refresh_token';
      $parsedBody['client_id']     = $passwordClient->id;
      $parsedBody['client_secret'] = $passwordClient->secret;
      $parsedBody['refresh_token'] = !empty($refreshTokenByRequest) ? $refreshTokenByRequest['refresh_token'] : request()->cookie('rt');

      try {
        $response = parent::issueToken($serverRequest->withParsedBody($parsedBody));
      } catch (Exception $exception) {
        throw new AuthenticationException('Invalid credentials');
      }

      $token = json_decode($response->getContent());

      return (new AuthResource($token))->additional([
        'expires_at' => Carbon::now()->addSeconds($token->expires_in)->format('d-m-Y H:i:s'),
      ]);
    } catch (Exception $exception) {
      $errorResponse = json_decode($exception->getMessage());
      if (isset($errorResponse->message)) {
        if ($errorResponse->message === "The refresh token is invalid.") {
          throw new AuthenticationException('Please relogin to continue!');
        }
      }
      throw $exception;
    }
  }

}
