<?php

namespace App\Services\API;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Exception;

class HelperService {

  /**
   * Upload file to storage folder
   * @param string $dir
   * @param object $data
   * @return string
   * @throws Exception
   */
  public function upload($dir, $data) {
    try {
      $fileName = $dir . '/' . md5($data->getClientOriginalName() . microtime()) . '-' . time() . '.' . $data->getClientOriginalExtension();
      Storage::disk('public')->put($fileName, File::get($data));
      return $fileName;
    } catch (Exception $exception) {
      throw new Exception($exception->getMessage());
    }
  }

}