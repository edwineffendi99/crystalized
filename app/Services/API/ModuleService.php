<?php

namespace App\Services\API;

use App\Http\Resources\API\Module\ModuleResource;
use App\Http\Resources\API\Role\RoleResource;
use App\Models\Module;
use App\Traits\RolePermission;
use Exception;

class ModuleService {

  /**
   * Find role
   * @throws Exception
   */
  public function __all() {
    try {
      return ModuleResource::collection(Module::with('permissions')->get());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}