<?php

namespace App\Services\API;

use Exception;
use Illuminate\Support\Facades\Auth;

class PermissionService {

  /**
   * Get list of permissions of current login user
   * @throws Exception
   */
  public function __all() {
    try {
      return Auth::guard('user_api')->user()->role->permissions->pluck('key');
    } catch (Exception $exception) {
      throw $exception;
    }
  }

}