<?php

namespace App\Services\API;

use App\Http\Resources\API\Role\RolePagination;
use App\Http\Resources\API\Role\RoleResource;
use App\Http\Resources\API\Role\SimpleRoleResource;
use App\Models\Role;
use App\Traits\RolePermission;
use Exception;
use Illuminate\Support\Facades\DB;

class RoleService {

  use RolePermission;

  protected string $module = 'role';

  /**
   * Get paginated role
   * @param integer $perPage
   * @param string $orderBy
   * @param string $orderSort
   * @return RolePagination
   * @throws Exception
   */
  public function __index(array $data, $perPage, $orderBy, $orderSort) {
    try {
      return new RolePagination(
        Role::when($data['name'], fn($q, $name) => $q->where('name', 'ilike', '%' . $name . '%'))
          ->when(isset($data['active']), fn($q, $active) => $q->where('active', $data['active']))
          ->orderBy($orderBy, $orderSort)
          ->paginate($perPage)
          ->appends($data));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Get all role
   * @throws Exception
   */
  public function __all() {
    try {
      return SimpleRoleResource::collection(Role::all());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Store role
   * @param array $data
   * @throws Exception
   */
  public function __store(array $data) {
    try {
      DB::beginTransaction();
      $role = Role::create([
        'name'   => $data['name'],
        'active' => $data['active']
      ]);
      $role->permissions()->attach($data['permissions_id']);
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Find role
   * @param string $skey
   * @return RoleResource
   * @throws Exception
   */
  public function __find(string $skey) {
    try {
      $role = Role::with('permissions')->where('skey', $skey)->firstOrFail();
      return new RoleResource($role);
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  /**
   * Update role
   * @param string $skey
   * @param array $data
   * @throws Exception
   */
  public function __update(array $data, string $skey) {
    try {
      DB::beginTransaction();
      $role = Role::where('skey', $skey)->firstOrFail();
      $role->update([
        'name'   => $data['name'],
        'active' => $data['active']
      ]);
      $role->permissions()->sync($data['permissions_id']);
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Delete role
   * @param string $skey
   * @throws Exception
   */
  public function __delete(string $skey) {
    try {
      DB::beginTransaction();
      $user = Role::where('skey', $skey)->firstOrFail();
      $user->delete();
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

  /**
   * Delete multiple role
   * @param array $skeys
   * @throws Exception
   */
  public function __deleteMulti(array $skeys) {
    try {
      DB::beginTransaction();
      $ids = Role::whereIn('skey', $skeys['skeys'])->pluck('id')->toArray();
      Role::destroy($ids);
      DB::commit();
    } catch (Exception $exception) {
      DB::rollBack();
      throw $exception;
    }
  }

}
