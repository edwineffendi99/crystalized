<?php

namespace App\Models;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;

class Role extends Model {
  protected $table      = 'roles';
  protected $guarded    = ['id'];
  public    $timestamps = true;

  public function scopeDefault() {
    $this->where('name', 'Default');
  }

  public function permissions() {
    return $this->belongsToMany(Permission::class, 'roles_permissions');
  }

  public function users() {
    return $this->hasMany(User::class);
  }

  public function rolePermission() {
    return $this->hasMany(RolePermission::class);
  }

  protected static function boot() {
    parent::boot();

    static::deleting(function ($role) {
      if($role->system_created){
        throw new AuthorizationException('System created Role cannot be deleted');
      }

      $role->users()->update(['role_id' => Role::default()->first()->id]);
    });
  }

}
