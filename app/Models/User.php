<?php

namespace App\Models;

use App\Traits\RolePermission as RolePermissionTraits;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {

  use HasApiTokens, RolePermissionTraits;

  protected $table      = 'users';
  protected $guarded    = ['id'];
  public    $timestamps = true;

  public const STATUS_INACTIVE = 0;
  public const STATUS_ACTIVE   = 1;

  public function setPasswordAttribute($value) {
    $this->attributes['password'] = Hash::make($value);
  }

  public function scopeActive($q) {
    $q->where('status', self::STATUS_ACTIVE);
  }

  public function scopeInactive($q) {
    $q->where('status', self::STATUS_INACTIVE);
  }

  public function role() {
    return $this->belongsTo(Role::class);
  }

  public function rolePermission() {
    return $this->hasManyThrough(RolePermission::class, Role::class, 'id', 'role_id');
  }

  protected static function boot() {
    parent::boot();

    static::deleting(function ($user) {
      if ($user->email == Auth::guard('user_api')->user()->email) {
        throw new AuthorizationException('Action forbidden');
      }
      if ($user->system_created) {
        throw new AuthorizationException('System created User cannot be deleted');
      }
    });
  }

}
