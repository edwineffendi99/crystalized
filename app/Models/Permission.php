<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {
  protected $table      = 'permissions';
  protected $guarded    = ['id'];
  public    $timestamps = true;

  public function roles() {
    return $this->belongsToMany(Role::class, 'roles_permissions');
  }

}
