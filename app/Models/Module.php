<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {
  protected $table      = 'modules';
  protected $guarded    = ['id'];
  public    $timestamps = true;

  public function scopeActive() {
    return $this->where('active', true);
  }

  public function permissions() {
    return $this->hasMany(Permission::class, 'module_id', 'id');
  }
}
