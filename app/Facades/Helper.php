<?php

namespace App\Facades;

use App\Services\API\HelperService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Helper
 * @method static upload(string $dir, object $data)
 * @see HelperService
 */

class Helper extends Facade {
  protected static function getFacadeAccessor() {
    return 'Helper';
  }
}