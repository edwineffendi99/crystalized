<?php

namespace App\Helpers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Routing\Route;
use Mpociot\ApiDoc\Extracting\Strategies\Strategy;
use ReflectionClass;
use ReflectionMethod;

class APIDocAddAuthorizationHeader extends Strategy {

  public function __invoke(Route $route, ReflectionClass $controller, ReflectionMethod $method, array $routeRules, array $context = []) {
    $user = User::first()->createToken('personal');

    return [
      'Authorization' => "Bearer $user->accessToken"
    ];
  }

}