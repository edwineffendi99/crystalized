<?php

if (!function_exists('responseSuccess')) {
  function responseSuccess($data = null, $message = '') {
    if ($message === '') {
      $message = 'Success';
    }

    if ($data instanceof \Illuminate\Http\Resources\Json\JsonResource) {
      if (empty($data->resource)) {
        $data = null;
      }
    }

    $response = [
      'status_code' => 200,
      'error_code'  => 0,
      'message'     => $message,
      'data'        => $data
    ];

    return response()->json($response, 200, [], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
  }
}

if (!function_exists('responseWithPagination')) {
  function responseWithPagination($data = [], $message = '') {
    if ($message === '') {
      $message = 'Success';
    }
    $response = array_merge(
      [
        'status_code' => 200,
        'error_code'  => 0,
        'message'     => $message,
      ],
      $data
    );
    return response()->json($response, 200, [], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
  }
}

if (!function_exists('responseError')) {
  function responseError($data = null, $message = '', $status = 404, $errorCode = 0) {

    if (!$status) {
      $status = 404;
    }

    if(!empty($data)){
      if (!is_array($data)) {
        $data = ['general' => $data];
      }
    }

    if ($message === '') {
      $message = 'Invalid';
    }
    $response = [
      'status_code' => $status,
      'error_code'  => $errorCode,
      'message'     => $message,
      'data'        => $data
    ];
    return response()->json($response, $status, [], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
  }
}