<?php

namespace App\Traits;

use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use ReflectionObject;

trait Logging {

  protected static function bootLogTraits() {

    static::creating(function ($model) {

      $user = self::__getUser();

      $changes = [
        'old' => null,
        'new' => $model->toArray()
      ];

      Log::insert([
        'type'       => 'CREATE',
        'causer_id'  => $user['id'],
        'causer'     => $user['username'],
        'subject_id' => $model->id,
        'subject'    => $model->getTable(),
        'changes'    => json_encode($changes),
        'created_at' => Carbon::now()->toDateTimeString()
      ]);

      //Log::channel($model->table)->info('created', array_merge($user, ['subject_id' => $model->id, 'subject' => $model->getTable(), 'changes' => $changes]));
    });

    static::updating(function ($model) {

      $user = self::__getUser();

      $changes = [];

      foreach ($model->getDirty() as $key => $value) {
        $original = $model->getOriginal($key);

        $old[$key] = $original;
        $new[$key] = $value;

        $changes = [
          'old' => $old,
          'new' => $new
        ];
      }

      Log::insert([
        'type'       => 'UPDATE',
        'causer_id'  => $user['id'],
        'causer'     => isset($user['username']) ? $user['username'] : $user['name'],
        'subject_id' => $model->id,
        'subject'    => $model->getTable(),
        'changes'    => json_encode($changes),
        'created_at' => Carbon::now()->toDateTimeString()
      ]);

      //Log::channel($model->table)->info('updated', array_merge($user, ['subject_id' => $model->id, 'subject' => $model->getTable(), 'changes' => $changes]));
    });

    static::deleting(function ($model) {

      $user = self::__getUser();

      $changes = [
        'old' => $model->toArray(),
        'new' => null
      ];

      Log::insert([
        'type'       => 'DELETE',
        'causer_id'  => $user['id'],
        'causer'     => isset($user['username']) ? $user['username'] : $user['name'],
        'subject_id' => $model->id,
        'subject'    => $model->getTable(),
        'changes'    => json_encode($changes),
        'created_at' => Carbon::now()->toDateTimeString()
      ]);

      //Log::channel($model->table)->info('deleted', array_merge($user, ['subject_id' => $model->id, 'subject' => $model->getTable()]));
    });

  }

  public function getLogList() {
    $files = glob(app_path() . '/Models/*.{php}', GLOB_BRACE);

    foreach ($files as $file) {
      $modelFile = 'App\Models\\' . basename($file, '.php');
      $model     = new $modelFile();

      $obj = new ReflectionObject($model);
      if ($obj->hasProperty('createLog')) {
        $refObj = $obj->getProperty('createLog');
        $refObj->setAccessible(TRUE);
        $createLog = $refObj->getValue($model);

        if ($createLog) {
          $refObj = $obj->getProperty('table');
          $refObj->setAccessible(TRUE);
          $table            = $refObj->getValue($model);
          $logsList[$table] = $table . '.log';
        }
      }
    }

    return $logsList;
  }

  public static function __getUser() {
    if (Auth::guard('web')->check()) {
      $logged = Auth::guard('web')->user();
      $user   = ['id' => $logged->id, 'username' => $logged->username];
    } else if (Auth::guard('member')->check()) {
      $logged = Auth::guard('member')->user();
      $user   = ['id' => $logged->id, 'username' => $logged->name];
    } else {
      $user = ['id' => null, 'username' => 'System'];
    }

    return $user;
  }
}