<?php

namespace App\Traits;

use App\Models\Module;
use App\Models\Permission;
use App\Models\Role;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

Trait RolePermission {

  public function permissions() {
    return [
      'role' => [
        'browse',
        'read',
        'edit',
        'add',
        'delete'
      ],
      'user' => [
        'browse',
        'read',
        'edit',
        'add',
        'delete'
      ],
    ];
  }

  public function generateModulePermissions($moduleKey) {
    if (!array_key_exists($moduleKey, $this->permissions())) {
      throw new Exception('Invalid module');
    }

    $permissions = [];
    foreach ($this->permissions()[$moduleKey] as $action) {
      $permissions[] = [
        'key'    => "$moduleKey:$action",
        'action' => $action
      ];
    }

    return $permissions;
  }

  public function assignModulePermission(Module $module) {
    try {
      $module->permissions()->createMany($this->generateModulePermissions($module->key));
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  public function assignRolePermissions(Role $role, Collection $permission) {
    try {
      $role->permissions()->attach($permission->pluck('id')->toArray());
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  public function getLoggedUser() {
    return Auth::guard('user_api')->user();
  }

  public function hasPermission($permission) {
    $user = $this->getLoggedUser();
    return (bool)$user->role->permissions->where('key', $permission->key)->count();
  }

  public function defineGates(){
    Permission::get()->map(function($permission){
      Gate::define($permission->key, function($user) use ($permission){
        return $user->hasPermission($permission);
      });
    });
  }

}