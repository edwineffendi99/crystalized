<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BackofficeTestCase extends TestCase {
  use DatabaseTransactions;

  public    $mockConsoleOutput = false;
  protected $loggedIn;

  public function setUp(): void {
    parent::setUp();

    $this->artisan('passport:client', ['--password' => true, '--no-interaction' => true]);
    $this->seed('ModulesTableSeeder');
    $this->seed('RolesTableSeeder');
    $this->seed('UsersTableSeeder');

    $this->loggedIn = User::first();
  }

}