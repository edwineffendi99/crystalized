<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\PassportTestCase;

class AuthenticationTest extends PassportTestCase {

  use RefreshDatabase;

  /**
   * @test
   * @group auth
   */
  public function it_login_with_valid_credential() {
    $this->post('/api/login', [
      'email'    => User::first()->email,
      'password' => 'password'
    ])
      ->assertOk()
      ->assertJsonStructure([
        'data' => [
          'access_token',
          'refresh_token',
          'token_type',
          'expires_at'
        ]
      ]);
  }

  /**
   * @test
   * @group auth
   */
  public function it_throw_error_when_login_with_invalid_credential() {
    $this->post('/api/login', [
      'email'    => 'mail@mail.com',
      'password' => 'passwords'
    ])
      ->assertStatus(401);
  }


  /**
   * @test
   * @group auth
   */
  public function it_show_logged_in_user_profile() {
    $this->get('/api/profile')
      ->assertStatus(200)
      ->assertJsonFragment([
        'name'        => $this->user->name,
        'email'       => $this->user->email,
        'active'      => $this->user->active,
        'profile_pic' => url('storage/' . $this->user->profile_pic),
      ]);
    $this->assertAuthenticated('user_api');
  }

  /**
   * @test
   * @group auth
   */
  public function it_refresh_token_when_token_expired() {
    $this->put('/api/refresh_token', [
      'refresh_token' => $this->refreshToken
    ])
      ->assertOk()
      ->assertJsonStructure([
        'data' => [
          'access_token',
          'refresh_token',
          'token_type',
          'expires_at'
        ]
      ]);
  }

  /**
   * @test
   * @group auth
   */
  public function it_logout() {
    $this->get('/api/logout')
      ->assertOk();
  }

}
