<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Tests\PassportTestCase;

class UserTest extends PassportTestCase {

  use RefreshDatabase, WithFaker;

  protected string $endpoint = '/api/users';

  /**
   * @test
   * @group user
   */
  public function it_return_paginated_users() {
    $user = User::orderBy('created_at', 'DESC')->first();
    $this->get("$this->endpoint/index/5")
      ->assertOk()
      ->assertJsonCount(5, 'data')
      ->assertJsonFragment([
        'name'  => $user->name,
        'email' => $user->email
      ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_return_paginated_filtered_users() {
    $email = 'admin@mail.com';
    $name  = 'admin';
    $this->get("$this->endpoint/index?email=$email&name=$name")
      ->assertOk()
      ->assertJsonFragment([
        'name'  => $this->user->name,
        'email' => $this->user->email
      ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_return_empty_paginated_filtered_users() {
    $this->get("$this->endpoint/index?email={$this->faker->email}&name={$this->faker->name}")
      ->assertOk()
      ->assertJsonCount(0, 'data')
      ->assertJsonFragment(['data' => []]);
  }

  /**
   * @test
   * @group user
   */
  public function it_create_a_new_user() {
    $role    = Role::first();
    $payload = [
      'name'        => $this->faker->name,
      'email'       => $this->faker->email,
      'password'    => 'password',
      'address'     => $this->faker->address,
      'role_id'     => $role->skey,
      'profile_pic' => UploadedFile::fake()->image('image.jpg'),
      'active'      => $this->faker->boolean
    ];

    $this->post($this->endpoint, $payload)
      ->assertOk();

    $this->assertDatabaseHas('users', [
      'name'    => $payload['name'],
      'email'   => $payload['email'],
      'address' => $payload['address'],
      'role_id' => $role->id,
      'active'  => $payload['active']
    ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_throw_validation_error_when_create_user_with_invalid_data() {

    $payload = [
      'name'        => $this->faker->email,
      'email'       => $this->faker->name,
      'password'    => 'password',
      'address'     => $this->faker->address,
      'role_id'     => $this->faker->uuid,
      'profile_pic' => UploadedFile::fake()->image('image.jpg'),
      'active'      => $this->faker->boolean
    ];

    $this->post($this->endpoint, $payload)
      ->assertStatus(422)
      ->assertJsonValidationErrors([
        'role_id',
        'email'
      ], 'data');

    $this->assertDatabaseMissing('users', Arr::only($payload, ['name', 'email', 'address']));
  }

  /**
   * @test
   * @group user
   */
  public function it_fetch_partial_user_by_name() {
    $this->get("$this->endpoint/fetch?name={$this->user->name}")
      ->assertOk()
      ->assertJsonFragment([
        'skey' => $this->user->skey,
        'name' => $this->user->name
      ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_fetch_partial_user_by_invalid_name() {
    $this->get("$this->endpoint/fetch?name={$this->faker->name}")
      ->assertOk()
      ->assertJsonFragment([
        'data' => []
      ])
      ->assertJsonCount(0, 'data');
  }

  /**
   * @test
   * @group user
   */
  public function it_find_user() {
    $this->get("$this->endpoint/{$this->user->skey}")
      ->assertOk()
      ->assertJsonFragment([
        'name'  => $this->user->name,
        'email' => $this->user->email
      ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_throw_error_when_find_invalid_user() {
    $this->get("$this->endpoint/13013eca-47d2-4bcb-b365-14d605f3d021")
      ->assertStatus(404)
      ->assertJsonFragment([
        'data' => []
      ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_update_user() {
    $role    = Role::first();
    $payload = [
      'name'        => $this->faker->name,
      'email'       => $this->faker->email,
      'address'     => $this->faker->address,
      'role_id'     => $role->skey,
      'profile_pic' => UploadedFile::fake()->image('image.jpg'),
      'active'      => $this->faker->boolean
    ];

    $this->patch("$this->endpoint/{$this->user->skey}", $payload)
      ->assertOk();

    $this->assertDatabaseHas('users', [
      'name'    => $payload['name'],
      'email'   => $payload['email'],
      'address' => $payload['address'],
      'role_id' => $role->id,
      'active'  => $payload['active']
    ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_throw_validation_error_when_update_user_with_invalid_data() {
    $payload = [
      'name'        => $this->faker->name,
      'email'       => $this->faker->state,
      'address'     => $this->faker->address,
      'role_id'     => $this->faker->uuid,
      'profile_pic' => UploadedFile::fake()->image('image.jpg'),
      'active'      => $this->faker->boolean
    ];

    $this->patch("$this->endpoint/{$this->user->skey}", $payload)
      ->assertStatus(422)
      ->assertJsonValidationErrors([
        'role_id',
        'email'
      ], 'data');

    $this->assertDatabaseMissing('users', Arr::only($payload, ['name', 'email', 'address', 'active']));
  }

  /**
   * @test
   * @group user
   */
  public function it_delete_user() {
    $user = User::where('system_created', false)->first();
    $this->delete("$this->endpoint/{$user->skey}")
      ->assertOk();

    $this->assertDatabaseMissing('users', [
      'name'    => $user->name,
      'email'   => $user->email,
      'address' => $user->address
    ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_unable_delete_system_created_user() {
    $user = User::where('system_created', true)->first();
    $this->delete("$this->endpoint/{$user->skey}")
      ->assertStatus(403);

    $this->assertDatabaseHas('users', [
      'name'    => $user->name,
      'email'   => $user->email,
      'address' => $user->address
    ]);
  }

  /**
   * @test
   * @group user
   */
  public function it_delete_multiple_user() {
    $skeys = User::where('system_created', false)->pluck('skey')->toArray();
    $this->post("$this->endpoint/delete_multi", [
      'skeys' => $skeys
    ])
      ->assertOk();

    $this->assertFalse(User::whereIn('skey', $skeys)->exists());
  }

  /**
   * @test
   * @group user
   */
  public function it_unable_delete_multiple_system_created_user() {
    $skeys = User::where('system_created', true)->pluck('skey')->toArray();
    $this->post("$this->endpoint/delete_multi", [
      'skeys' => $skeys
    ])
      ->assertStatus(403);

    $this->assertTrue(User::whereIn('skey', $skeys)->exists());
  }

}
