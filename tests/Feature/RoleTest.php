<?php

namespace Tests\Feature;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\PassportTestCase;

class RoleTest extends PassportTestCase {

  use RefreshDatabase, WithFaker;

  protected string $endpoint = '/api/roles';

  /**
   * @test
   * @group role
   */
  public function it_return_paginated_roles() {
    $role = Role::first();
    $this->get("$this->endpoint/index/3")
      ->assertOk()
      ->assertJsonCount(3, 'data')
      ->assertJsonFragment([
        'name' => $role->name
      ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_return_paginated_filtered_roles() {
    $name = 'Administrator';
    $this->get("$this->endpoint/index?name=$name")
      ->assertOk()
      ->assertJsonFragment([
        'name' => $name
      ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_return_empty_paginated_filtered_roles() {
    $this->get("$this->endpoint/index?name={$this->faker->name}")
      ->assertOk()
      ->assertJsonCount(0, 'data')
      ->assertJsonFragment(['data' => []]);
  }

  /**
   * @test
   * @group role
   */
  public function it_create_a_new_role() {
    $payload = [
      'name'           => $this->faker->name,
      'active'         => $this->faker->boolean,
      'permissions_id' => Permission::all()->pluck('id')->toArray()
    ];

    $this->post($this->endpoint, $payload)
      ->assertOk();

    $role = Role::where([
      ['name', $payload['name']],
      ['active', $payload['active']]
    ])->first();

    $this->assertNotNull($role);
    $this->assertEquals(Arr::only($payload, ['name', 'active']), $role->only(['name', 'active']));

    $this->assertDatabaseHas('roles_permissions', [
      'role_id'       => $role->id,
      'permission_id' => $payload['permissions_id'][0]
    ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_throw_validation_error_when_create_role_with_invalid_data() {

    $payload = [
      'name'           => $this->faker->name,
      'active'         => $this->faker->boolean,
      'permissions_id' => Arr::wrap(1000)
    ];

    $this->post($this->endpoint, $payload)
      ->assertStatus(422)
      ->assertJsonValidationErrors([
        'permissions_id.0',
      ], 'data');

    $this->assertDatabaseMissing('roles', Arr::only($payload, ['name', 'active']));
  }

  /**
   * @test
   * @group role
   */
  public function it_find_role() {
    $role = Role::with('permissions')->first();
    $this->get("$this->endpoint/{$role->skey}")
      ->assertOk()
      ->assertJsonFragment([
        'name'        => $role->name,
        'active'      => $role->active,
        'permissions' => $role->permissions->pluck('id')->toArray()
      ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_throw_error_when_find_invalid_role() {
    $this->get("$this->endpoint/13013eca-47d2-4bcb-b365-14d605f3d021")
      ->assertStatus(404)
      ->assertJsonFragment([
        'data' => []
      ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_update_role() {
    $role    = Role::first();
    $payload = [
      'name'           => $this->faker->name,
      'active'         => $this->faker->boolean,
      'permissions_id' => Permission::all()->pluck('id')->toArray()
    ];

    $this->patch("$this->endpoint/{$role->skey}", $payload)
      ->assertOk();

    $role = Role::where([
      ['name', $payload['name']],
      ['active', $payload['active']]
    ])->first();

    $this->assertNotNull($role);
    $this->assertEquals(Arr::only($payload, ['name', 'active']), $role->only(['name', 'active']));

    $this->assertDatabaseHas('roles_permissions', [
      'role_id'       => $role->id,
      'permission_id' => $payload['permissions_id'][0]
    ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_throw_validation_error_when_update_role_with_invalid_data() {
    $role    = Role::first();
    $payload = [
      'name'           => $this->faker->name,
      'active'         => $this->faker->boolean,
      'permissions_id' => Arr::wrap(1000)
    ];

    $this->patch("$this->endpoint/{$role->skey}", $payload)
      ->assertStatus(422)
      ->assertJsonValidationErrors([
        'permissions_id.0',
      ], 'data');

    $this->assertDatabaseMissing('roles', Arr::only($payload, ['name', 'active']));
  }

  /**
   * @test
   * @group role
   */
  public function it_delete_role() {
    $role = Role::where('system_created', false)->first();
    $this->delete("$this->endpoint/{$role->skey}")
      ->assertOk();

    $this->assertDatabaseMissing('roles', [
      'name'   => $role->name,
      'active' => $role->active
    ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_unable_delete_system_created_role() {
    $role = Role::where('system_created', true)->first();
    $this->delete("$this->endpoint/{$role->skey}")
      ->assertStatus(403);

    $this->assertDatabaseHas('roles', [
      'name'   => $role->name,
      'active' => $role->active
    ]);
  }

  /**
   * @test
   * @group role
   */
  public function it_delete_multiple_role() {
    $skeys = Role::where('system_created', false)->pluck('skey')->toArray();
    $this->post("$this->endpoint/delete_multi", [
      'skeys' => $skeys
    ])
      ->assertOk();

    $this->assertFalse(Role::whereIn('skey', $skeys)->exists());
  }

  /**
   * @test
   * @group role
   */
  public function it_unable_delete_multiple_system_created_role() {
    $skeys = Role::where('system_created', true)->pluck('skey')->toArray();
    $this->post("$this->endpoint/delete_multi", [
      'skeys' => $skeys
    ])
      ->assertStatus(403);

    $this->assertTrue(Role::whereIn('skey', $skeys)->exists());
  }

}
