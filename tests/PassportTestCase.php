<?php

namespace Tests;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\ClientRepository;
use App\Traits\RolePermission;

class PassportTestCase extends TestCase {
  use DatabaseTransactions, RolePermission;

  protected $headers      = [];
  protected $scopes       = [];
  protected $user;
  protected $accessToken  = '';
  protected $refreshToken = '';

  public function setUp(): void {
    parent::setUp();
    $clientRepository = new ClientRepository();
    $client           = $clientRepository->createPasswordGrantClient(
      null, 'Test Password Grant Client', url('/')
    );

    $this->seed('ModulesTableSeeder');
    $this->seed('RolesTableSeeder');
    $this->seed('UsersTableSeeder');

    $this->user = User::first();

    $response = $this->post('/oauth/token', [
      'grant_type'    => 'password',
      'client_id'     => $client->id,
      'client_secret' => $client->secret,
      'username'      => $this->user->email,
      'password'      => 'password'
    ]);

    $token = (json_decode($response->getContent()));

    $this->headers['Accept']        = 'application/json';
    $this->headers['Authorization'] = 'Bearer ' . $token->access_token;

    // define gates for user role permissions
    $this->defineGates();

    $this->accessToken  = $token->access_token;
    $this->refreshToken = $token->refresh_token;
  }

  public function get($uri, array $headers = []) {
    return parent::get($uri, array_merge($this->headers, $headers));
  }

  public function getJson($uri, array $headers = []) {
    return parent::getJson($uri, array_merge($this->headers, $headers));
  }

  public function post($uri, array $data = [], array $headers = []) {
    return parent::post($uri, $data, array_merge($this->headers, $headers));
  }

  public function postJson($uri, array $data = [], array $headers = []) {
    return parent::postJson($uri, $data, array_merge($this->headers, $headers));
  }

  public function put($uri, array $data = [], array $headers = []) {
    return parent::put($uri, $data, array_merge($this->headers, $headers));
  }

  public function putJson($uri, array $data = [], array $headers = []) {
    return parent::putJson($uri, $data, array_merge($this->headers, $headers));
  }

  public function patch($uri, array $data = [], array $headers = []) {
    return parent::patch($uri, $data, array_merge($this->headers, $headers));
  }

  public function patchJson($uri, array $data = [], array $headers = []) {
    return parent::patchJson($uri, $data, array_merge($this->headers, $headers));
  }

  public function delete($uri, array $data = [], array $headers = []) {
    return parent::delete($uri, $data, array_merge($this->headers, $headers));
  }

  public function deleteJson($uri, array $data = [], array $headers = []) {
    return parent::deleteJson($uri, $data, array_merge($this->headers, $headers));
  }
}