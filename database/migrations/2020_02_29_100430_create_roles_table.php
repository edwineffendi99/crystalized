<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    Schema::create('roles', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->uuid('skey')->unique();
      $table->string('name');
      $table->boolean('active');
      $table->boolean('system_created')->default(false);
      $table->timestamps();
    });
    DB::statement('ALTER TABLE roles ALTER COLUMN skey SET DEFAULT uuid_generate_v4();');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('roles');
  }
}
