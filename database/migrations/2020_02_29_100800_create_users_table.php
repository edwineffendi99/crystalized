<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    Schema::create('users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->uuid('skey')->unique();
      $table->string('name');
      $table->string('email')->unique();
      $table->string('address')->nullable();
      $table->string('profile_pic')->nullable();
      $table->unsignedInteger('role_id');
      $table->boolean('active')->default(false);
      $table->timestamp('email_verified_at')->nullable();
      $table->string('password');
      $table->boolean('system_created')->default(false);
      $table->rememberToken();
      $table->timestamps();

      $table->foreign('role_id')->references('id')->on('roles')
        ->onUpdate('cascade');
    });
    DB::statement('ALTER TABLE users ALTER COLUMN skey SET DEFAULT uuid_generate_v4();');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('users');
  }
}
