<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    Schema::create('modules', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->uuid('skey')->unique();
      $table->string('name');
      $table->string('key');
      $table->boolean('active');
      $table->unsignedInteger('parent_id')->nullable();
      $table->unsignedInteger('priority')->default(99);
      $table->string('route');
      $table->timestamps();
    });
    DB::statement('ALTER TABLE modules ALTER COLUMN skey SET DEFAULT uuid_generate_v4();');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('modules');
  }
}
