<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('logs', function (Blueprint $table) {
      $table->id();
      $table->string('module_id');
      $table->string('subject')->nullable();
      $table->text('causer_id')->nullable();
      $table->string('causer')->nullable();
      $table->text('changes')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('logs');
  }
}
