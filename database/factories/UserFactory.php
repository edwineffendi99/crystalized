<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Role;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
  return [
    'name'           => $faker->firstName,
    'email'          => $faker->email,
    'password'       => 'password',
    'address'        => $faker->address,
    'profile_pic'    => 'users/' . $faker->randomElement(['9.jpg', '11.jpg', '93.jpg']),
    'active'         => $faker->boolean,
    'system_created' => $faker->boolean,
    'role_id'        => factory(Role::class)
  ];
});
