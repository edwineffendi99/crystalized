<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
  return [
    'skey'           => $faker->uuid,
    'name'           => $faker->jobTitle,
    'active'         => $faker->boolean,
    'system_created' => $faker->boolean,
  ];
});
