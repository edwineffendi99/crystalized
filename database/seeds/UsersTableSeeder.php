<?php

use App\Models\Role;
use App\Models\User;
use App\Traits\RolePermission;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
  use RolePermission;

  /**
   * Run the database seeds.
   * @return void
   * @throws Exception
   */
  public function run() {

    try {
      $support = Role::where('name', 'Support')->firstOrFail();
      $role    = Role::where('name', 'Administrator')->firstOrFail();
    } catch (Exception $exception) {
      throw new Exception($exception->getMessage());
    }

    factory(User::class)->create([
      'skey'           => '13013eca-47d2-4bcb-b365-14d605f3d037',
      'email'          => 'admin@mail.com',
      'name'           => 'admin',
      'password'       => 'password',
      'address'        => 'Bogor',
      'profile_pic'    => 'users/9.jpg',
      'role_id'        => $role->id,
      'active'         => true,
      'system_created' => true,
    ]);

    factory(User::class)->create([
      'skey'           => '13013eca-47d2-4bcb-b365-14d605f3d038',
      'email'          => 'support@mail.com',
      'name'           => 'Tech Support',
      'password'       => 'password',
      'address'        => 'England',
      'profile_pic'    => 'users/9.jpg',
      'role_id'        => $support->id,
      'active'         => true,
      'system_created' => false,
    ]);

    factory(User::class, 10)->create([
      'role_id'        => $support->id,
      'system_created' => false
    ]);
  }
}
