<?php

use App\Models\Permission;
use App\Models\Role;
use App\Traits\RolePermission;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {
  use RolePermission;

  /**
   * Run the database seeds.
   * @return void
   * @throws Exception
   */
  public function run() {

    try {
      $administrator = factory(Role::class)->create([
        'skey'           => '13013eca-47d2-4bcb-b365-14d605f3d037',
        'name'           => 'Administrator',
        'active'         => true,
        'system_created' => true,
      ]);

      $support = factory(Role::class)->create([
        'skey'           => '13013eca-47d2-4bcb-b365-14d605f3d038',
        'name'           => 'Support',
        'active'         => true,
        'system_created' => false
      ]);

      $default = factory(Role::class)->create([
        'skey'           => '13013eca-47d2-4bcb-b365-14d605f3d039',
        'name'           => 'Default',
        'active'         => true,
        'system_created' => true
      ]);

      $this->assignRolePermissions($administrator, Permission::all());
      $this->assignRolePermissions($support, Permission::whereIn('action', ['browse', 'read'])->get());
      $this->assignRolePermissions($default, Permission::where('action', 'browse')->get());
    } catch (Exception $exception) {
      throw new Exception($exception->getMessage());
    }
  }
}
