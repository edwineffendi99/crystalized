<?php

use App\Models\Module;
use App\Traits\RolePermission;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder {
  use RolePermission;

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    try {
      $module = Module::firstOrCreate(['key' => 'user'], [
        'name'      => 'User',
        'key'       => 'user',
        'active'    => true,
        'parent_id' => null,
        'priority'  => 1,
        'route'     => 'user'
      ]);
      $this->assignModulePermission($module);

      $module = Module::firstOrCreate(['key' => 'role'], [
        'name'      => 'Role',
        'key'       => 'role',
        'active'    => true,
        'parent_id' => null,
        'priority'  => 2,
        'route'     => 'role'
      ]);
      $this->assignModulePermission($module);
    } catch (Exception $exception) {

    }

  }
}
