<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', ['uses' => 'API\AuthController@login']);
Route::put('refresh_token', ['uses' => 'API\AuthController@refreshToken']);
Route::get('logout', 'API\AuthController@logout');

Route::group(['prefix' => '/', 'middleware' => ['userCookieAuth', 'auth:user_api', 'api.csrf', 'check.authenticated'], 'namespace' => 'API'], function () {
  Route::get('profile', 'AuthController@profile');

  Route::group(['prefix' => 'permissions', 'middleware' => []], function () {
    Route::get('all', 'PermissionController@all');
  });

  Route::group(['prefix' => 'users', 'middleware' => []], function () {
    Route::get('index/{perPage?}/{orderBy?}/{sortOrder?}', 'UserController@index');
    Route::get('fetch', 'UserController@fetch');
    Route::get('all', 'UserController@all');
    Route::post('/', 'UserController@store');
    Route::get('{skey}', 'UserController@find');
    Route::patch('{skey}', 'UserController@update');
    Route::delete('{skey}', 'UserController@delete');
    Route::post('delete_multi', 'UserController@deleteMulti');
  });

  Route::group(['prefix' => 'roles', 'middleware' => []], function () {
    Route::get('index/{perPage?}/{orderBy?}/{sortOrder?}', 'RoleController@index');
    Route::get('all', 'RoleController@all');
    Route::post('/', 'RoleController@store');
    Route::get('{skey}', 'RoleController@find');
    Route::patch('{skey}', 'RoleController@update');
    Route::delete('{skey}', 'RoleController@delete');
    Route::post('delete_multi', 'RoleController@deleteMulti');
  });

  Route::group(['prefix' => 'modules', 'middleware' => []], function () {
    Route::get('all', 'ModuleController@all');
  });
});
